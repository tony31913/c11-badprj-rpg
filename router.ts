import express from 'express';

//controller
import {playerController, battleController, itemController, skillController} from './main'

export const routes = express.Router();



//route here
routes.post('/account',playerController.signUp)
routes.post('/login',playerController.logIn)
routes.post('/faceDetection',playerController.faceDetection)
routes.post('/select',playerController.select)
routes.get('/logout',playerController.logout)
routes.put('/savaData',playerController.saveData)

//battle Related Routes
routes.get('/loadData',battleController.loadData)
routes.get('/loadAllMonster',battleController.loadAllMonster)
routes.put('/loadMonster',battleController.loadMonster)
routes.put('/autoAttack',battleController.autoAttack)
routes.put('/counterAttack',battleController.counterAttack)
routes.put('/escapeFunction',battleController.escapeFunction)
routes.put('/battleUseItem',battleController.battleUseItem)

//item related Routes
routes.get('/loadCharItems', itemController.loadCharItems)
routes.get('/loadShopItems', itemController.loadItemsList)
routes.get('/checkCharGold',itemController.checkCharCurrency)
routes.post('/buyItems', itemController.buyItems)
routes.put('/updateHpMp', itemController.updateHpMp)
routes.put('/updateEquippedStatus', itemController.updateEquippedStatus)
routes.put('/useItems', itemController.usePotion)

//skill related Routes
routes.get('/loadSkill',skillController.loadSkill)
routes.get('/loadAllSkills',skillController.loadAllSkills)
routes.put('/useSkill',skillController.useSkill)