import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable('monsters_list')){
        return 
    }
    await knex.schema.createTable('monsters_list', table=> {
        table.increments()
        table.string('name').notNullable().unique()
        table.integer('hp').notNullable()
        table.integer('atk').notNullable()
        table.integer('def').notNullable()
        table.boolean('alive').notNullable()
        table.string('location').notNullable()
        table.string('img').notNullable()
        table.integer('expPoint').notNullable()
        table.integer('gold').notNullable()
        
    })
}


export async function down(knex: Knex): Promise<void> {
    return await knex.schema.dropTableIfExists('monsters_list');
}
