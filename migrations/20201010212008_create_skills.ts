import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable('skills')){
        return 
    }
    await knex.schema.createTable('skills', table=> {
        table.increments('id')
        table.string('name').notNullable().unique()
        table.string('content')
        table.decimal('damage',3,2)
        table.integer('cost').notNullable()
    })
}


export async function down(knex: Knex): Promise<void> {
    return await knex.schema.dropTableIfExists('skills');
}

