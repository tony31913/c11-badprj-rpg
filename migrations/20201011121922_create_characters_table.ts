import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable('characters')){
        return 
    }
    await knex.schema.createTable('characters', table=> {
        table.increments()
        table.integer('level').notNullable()
        table.integer('exp').notNullable()
        table.integer('gold').notNullable()
        table.integer('hp').notNullable()
        table.integer('mp').notNullable()
        table.integer('atk').notNullable()
        table.integer('def').notNullable()
        table.string('location').notNullable()
        table.string('img').notNullable()
        table.string('name').notNullable()
        table.integer('occupation_id').unsigned()
        table.integer('player_id').unsigned()
        table.foreign('player_id').references('player.id')
        table.boolean('alive').notNullable()
        table.integer('round').notNullable()
        table.integer('skill_1')
        table.integer('skill_2')
        table.integer('skill_3')
        table.integer('skill_4')
    })
}


export async function down(knex: Knex): Promise<void> {
    return await knex.schema.dropTableIfExists('characters');
}

