import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable('monsters')){
        return 
    }
    await knex.schema.createTable('monsters', table=> {
        table.increments()
        table.integer('character_id').notNullable()
        table.foreign('character_id').references('characters.id')
        table.string('name').notNullable()
        table.integer('monsters_list_id').notNullable()
        table.foreign('monsters_list_id').references('monsters_list.id')
        table.integer('hp').notNullable()
        table.integer('atk').notNullable()
        table.integer('def').notNullable()
        table.integer('expPoint').notNullable()
        table.integer('gold').notNullable()
        table.boolean('alive').notNullable()
        table.string('location').notNullable()
        table.string('img').notNullable()
    
        
    })
}


export async function down(knex: Knex): Promise<void> {
    return await knex.schema.dropTableIfExists('monsters');
}

