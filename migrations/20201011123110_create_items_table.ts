import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable('items')){
        return 
    }
    await knex.schema.createTable('items', table=> {
        table.increments()
        table.string('name').notNullable().unique()
        table.integer('def').notNullable()
        table.integer('atk').notNullable()
        table.integer('gold').notNullable()
        table.string('type').notNullable()
        table.boolean('consumable').notNullable().defaultTo(false)
        table.string('image_index').unique()
    })
}


export async function down(knex: Knex): Promise<void> {
    return await knex.schema.dropTableIfExists('items');
}
