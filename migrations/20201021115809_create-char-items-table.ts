import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable('char-items')) {
        return
    }
    await knex.schema.createTable('char-items', table=> {
        table.increments()
        table.integer('quantity').notNullable()
        table.integer('character_id').unsigned()
        table.foreign('character_id').references('characters.id')
        table.integer('item_id').unsigned()
        table.foreign('item_id').references('items.id')
        table.boolean('equipped').defaultTo(false)
    })
}


export async function down(knex: Knex): Promise<void> {
    return await knex.schema.dropTableIfExists('char-items');
}