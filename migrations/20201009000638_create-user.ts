import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('player');
    if(!hasTable){
        return knex.schema.createTable('player',(table)=>{
            table.increments();
            table.string("username").notNullable().unique();
            table.string("display_name").notNullable().unique();
            table.string("password").notNullable();
            table.timestamps(false,true);
        });  
    }else{
        return Promise.resolve();
    }
};


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTableIfExists('player');
}

