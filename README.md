# Role-playing Game

A tile-based RPG mini-game which allows users to select occupation for their adventure

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

You need to install npm or yarn, PostgreSQL, <code>Redis, Python3.7 and related packages</code> in your local machine to run this project.

### Installing

You may run below command to install all packages:

```bash
yarn install
```

or

```bash
npm install
```

### Setting Environment

You may follow .env.sample in the folder to create your .env file.

You need to create a database in your PostgreSQL, and fill in the following in your .env file:

example:

```
DB_NAME=your_database_name
DB_USERNAME=your_username
DB_PASSWORD=your_password
```

Then, you need to run below commands to create tables and insert records in your database.

```bash
yarn knex migrate:latest
yarn knex seed:run
```

or

```bash
npx knex migrate:latest
npx knex seed:run
```

### Starting the Program

You may run below command to start PostgreSQL:

```bash
sudo service postgresql start
```

You may run below command to start Redis:

```bash
redis-server
```

you may run below command to start Python server:

```
python app.py
```

You may run below command to start the program:

```
ts-node-dev main.ts
```

or

```
ts-node main.ts
```

## Running the Tests

You may run the automated tests by below command:

```bash
yarn jest
```

or

```bash
npx jest
```

## Authors

This project is done by below students from Tecky Academy:

- Wong Kwong Fat, Tony
- Tsui Kin Hang, Ken
- Wong Wai Kit, Jack

## License

This project is licensed under the MIT License.
