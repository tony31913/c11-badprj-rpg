import tornado.ioloop
import tornado.web
import tensorflow as tf
import numpy as np
from tensorflow import keras
import random
import string
import os

class_names = ('sad', 'neutral', 'happy')


class MainHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        print("setting headers!!!")
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "content-type")
        self.set_header('Access-Control-Allow-Methods',
                        'POST, GET, OPTIONS, PATCH, PUT')
        self.set_header("Content-Type", 'application/json')

    def post(self):
        model = tf.saved_model.load(
            './ai-related/my-model')
        print('model loaded')
        file = self.request.files["image"][0]
        print(type(file))
        fname = ''.join(random.choice(string.ascii_lowercase +
                        string.digits) for x in range(6)) + '.png'
        outputFile = open("tmp/" + fname, 'wb')
        outputFile.write(file['body'])
        img = tf.keras.preprocessing.image.load_img(
            "tmp/" + fname, color_mode="grayscale", target_size=(48, 48))
        x = tf.keras.preprocessing.image.img_to_array(img)
        x = np.expand_dims(x, axis=0)
        x /= 255

        # print(x)
        os.remove("tmp/" + fname)

        result = model(x, training=False)
        print(result[0])

        max = 0.000000000000000000001
        prediction = result[0]
        for i in range(0, len(prediction)):
            if prediction[i] > max:
                max = prediction[i]
                ind = i

        print('Expression Prediction:', class_names[ind])

        self.write({"data": class_names[ind]})


def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
    ])


if __name__ == "__main__":
    app = make_app()
    server = tornado.httpserver.HTTPServer(app)
    server.bind(8888)
    server.start(2)
    tornado.ioloop.IOLoop.current().start()
