import { Request, Response } from "express";
import { BattleService } from "../service/BattleService";
import redis from "redis";
import util from "util";
import { bossLogic } from "../function";
import { allitemsList } from "../items";

export class BattleController {
  constructor(
    public battleService: BattleService,
    public client: redis.RedisClient
  ) {}

  loadData = async (req: Request, res: Response) => {
    const playerId = req.session?.user.id;
    const data = await this.battleService.loadData(playerId);
    const equippedValue = await this.battleService.loadEquippedItems(playerId);
    const redisSet = util.promisify(this.client.hset).bind(this.client);

    const hp = data[0].hp;
    let atk = data[0].atk;
    let def = data[0].def;
    const mp = data[0].mp;
    if (equippedValue != undefined) {
      atk += +equippedValue[0];
      def += +equippedValue[1];
    }

    await redisSet(`${playerId}`, "hp", `${hp}`);
    await redisSet(`${playerId}`, "mp", `${mp}`);
    await redisSet(`${playerId}`, "atk", `${atk}`);
    await redisSet(`${playerId}`, "def", `${def}`);

    return res.json(data);
  };

  loadMonster = async (req: Request, res: Response) => {
    const playerId = req.session?.user.id;
    const monsterId = req.body.monsterId;
    const redisSet = util.promisify(this.client.hset).bind(this.client);

    const data = await this.battleService.loadMonster(playerId, monsterId);

    const hp = data[0].hp;
    const atk = data[0].atk;
    const def = data[0].def;

    await redisSet(`${playerId}_${monsterId}`, "hp", `${hp}`);
    await redisSet(`${playerId}_${monsterId}`, "fullhp", `${hp}`);
    await redisSet(`${playerId}_${monsterId}`, "atk", `${atk}`);
    await redisSet(`${playerId}_${monsterId}`, "def", `${def}`);

    return res.json(data);
  };

  loadAllMonster = async (req: Request, res: Response) => {
    const playerId = req.session?.user.id;
    const data = await this.battleService.loadAllMonster(playerId);

    return res.json(data);
  };

  autoAttack = async (req: Request, res: Response) => {
    const playerId = req.session?.user.id;

    const monsterId = req.body.monsterId;

    const redisGet = util.promisify(this.client.hget).bind(this.client);
    const redisSet = util.promisify(this.client.hset).bind(this.client);

    const playerAtk = +(await redisGet(`${playerId}`, "atk"));
    // const playerHp = +await redisGet(`${playerId}`, 'hp')
    // const playerMp = +await redisGet(`${playerId}`, 'mp')
    // const playerDef = +await redisGet(`${playerId}`, 'def')

    const monsterAtk = +(await redisGet(`${playerId}_${monsterId}`, "atk"));
    const monsterHp = +(await redisGet(`${playerId}_${monsterId}`, "hp"));
    const monsterDef = +(await redisGet(`${playerId}_${monsterId}`, "def"));

    //attack monster logic here
    const damage = playerAtk - monsterDef;

    if (damage > 0) {
      await redisSet(`${playerId}_${monsterId}`, "hp", `${monsterHp - damage}`);
    }

    const latestPlayerHp = await redisGet(`${playerId}`, "hp");
    const latestMonsterHp = await redisGet(`${playerId}_${monsterId}`, "hp");
    const latestPlayerMp = +(await redisGet(`${playerId}`, "mp"));

    if (latestMonsterHp <= 0) {
      const result = await this.updateAfterWin(
        playerId,
        latestPlayerHp,
        latestPlayerMp,
        playerAtk,
        monsterId
      );

      return res.json(result);
      // const data = await this.battleService.saveBattle(playerId, latestPlayerHp, monsterId)
      // await this.battleService.checkLevel(playerId)
      // return res.json([{ hp: latestPlayerHp, atk: playerAtk, id: playerId, location: data.location, expPoint: data.expPoint }, { hp: 0, atk: monsterAtk, id: monsterId, alive: false }])
    } else {
      return res.json([
        {
          hp: latestPlayerHp,
          mp: latestPlayerMp,
          atk: playerAtk,
          id: playerId,
        },
        { hp: latestMonsterHp, atk: monsterAtk, id: monsterId },
      ]);
    }
  };

  updateAfterWin = async (
    playerId: number,
    latestPlayerHp: number,
    latestPlayerMp: number,
    playerAtk: number,
    monsterId: number
  ) => {
    const data = await this.battleService.saveBattle(
      playerId,
      latestPlayerHp,
      latestPlayerMp,
      monsterId
    );
    const result = await this.battleService.checkLevel(playerId);

    return [
      {
        hp: latestPlayerHp,
        mp: latestPlayerMp,
        id: playerId,
        location: data[0].location,
        expPoint: data[0].expPoint,
        pastLevel: data[1][0],
        newLevel: result,
        atk: playerAtk,
      },
      { hp: 0, id: monsterId, alive: false },
    ];
  };

  counterAttack = async (req: Request, res: Response) => {
    const playerId = req.session?.user.id;
    const occId = await this.battleService.checkOccupation(playerId);
    const monsterId = req.body.monsterId;
    const monsterListId = await this.battleService.checkMonsterListId(
      monsterId
    );

    const redisGet = util.promisify(this.client.hget).bind(this.client);
    const redisSet = util.promisify(this.client.hset).bind(this.client);

    const playerHp = +(await redisGet(`${playerId}`, "hp"));
    const playerMp = +(await redisGet(`${playerId}`, "mp"));
    const playerDef = +(await redisGet(`${playerId}`, "def"));
    const monsterAtk = +(await redisGet(`${playerId}_${monsterId}`, "atk"));
    const monsterHp = +(await redisGet(`${playerId}_${monsterId}`, "hp"));
    const monsterFullHp = +(await redisGet(
      `${playerId}_${monsterId}`,
      "fullhp"
    ));

    let currentSkill: string;
    let finalDamage: number;

    //counter attack
    const monsterHalfHp = monsterFullHp / 2;
    switch (monsterListId[0].monsters_list_id) {
      case 10:
        const result = bossLogic(
          occId[0].occupation_id,
          monsterAtk,
          monsterFullHp,
          monsterHp
        );

        switch (result) {
          case "Normal Attack":
            const random = Math.random();
            const damage = Math.round(monsterAtk - playerDef);
            if (random < 0.7) {
              currentSkill = "Normal Attack";
              if (damage > 0) {
                await redisSet(`${playerId}`, "hp", `${playerHp - damage}`);
                finalDamage = damage;
              } else {
                await redisSet(`${playerId}`, "hp", `${playerHp - 1}`);
                finalDamage = 1;
              }
            } else if (random < 0.98 && random >= 0.7) {
              const damage = Math.round(monsterAtk * 2 - playerDef);
              currentSkill = "Crit Attack";
              if (damage > 0) {
                await redisSet(`${playerId}`, "hp", `${playerHp - damage}`);
                finalDamage = damage;
              } else {
                await redisSet(`${playerId}`, "hp", `${playerHp - 1}`);
                finalDamage = 1;
              }
            } else {
              currentSkill = "Miss";
              await redisSet(`${playerId}`, "hp", `${playerHp - 1}`);
              finalDamage = 1;
            }
            break;
          case "Hp Draw":
            const damage1 = Math.round(monsterAtk - playerDef);
            currentSkill = "Blood Sucking";
            if (damage1 > 0) {
              await redisSet(`${playerId}`, "hp", `${playerHp - damage1}`);
              await redisSet(
                `${playerId}_${monsterId}`,
                "hp",
                `${monsterHp + damage1}`
              );
              finalDamage = damage1;
            } else {
              await redisSet(`${playerId}`, "hp", `${playerHp - 1}`);
              await redisSet(
                `${playerId}_${monsterId}`,
                "hp",
                `${monsterHp + 1}`
              );
              finalDamage = 1;
            }
            break;
          case "Mp Draw":
            const damage2 = Math.round(monsterAtk - playerDef);
            currentSkill = "Exhaustion";
            if (damage2 > 0) {
              await redisSet(`${playerId}`, "hp", `${playerHp - damage2}`);
              await redisSet(`${playerId}`, "mp", `${playerMp - 20}`);
              finalDamage = damage2;
            } else {
              await redisSet(`${playerId}`, "hp", `${playerHp - 1}`);
              await redisSet(`${playerId}`, "mp", `${playerMp - 20}`);
              finalDamage = 1;
            }
            break;
          case "Enerage":
            const realDamage = Math.round(monsterAtk * 3);
            currentSkill = "Final Blow";
            if (realDamage > 0) {
              await redisSet(`${playerId}`, "hp", `${playerHp - realDamage}`);
              finalDamage = realDamage;
            } else {
              await redisSet(`${playerId}`, "hp", `${playerHp - 1}`);
              finalDamage = 1;
            }
            break;
        }

        break;
      default:
        if (monsterHp < monsterHalfHp) {
          const damage = Math.round(monsterAtk * 2 - playerDef);
          currentSkill = "Crit Attack";
          if (damage > 0) {
            await redisSet(`${playerId}`, "hp", `${playerHp - damage}`);
            finalDamage = damage;
          } else {
            await redisSet(`${playerId}`, "hp", `${playerHp - 1}`);
            finalDamage = 1;
          }
        } else {
          const random = Math.random();
          if (random < 0.7) {
            const damage = Math.round(monsterAtk - playerDef);
            currentSkill = "Normal Attack";

            if (damage > 0) {
              await redisSet(`${playerId}`, "hp", `${playerHp - damage}`);

              finalDamage = damage;
            } else {
              await redisSet(`${playerId}`, "hp", `${playerHp - 1}`);
              finalDamage = 1;
            }
          } else if (random < 0.98 && random >= 0.7) {
            const damage = Math.round(monsterAtk * 2 - playerDef);
            currentSkill = "Crit Attack";
            if (damage > 0) {
              await redisSet(`${playerId}`, "hp", `${playerHp - damage}`);
              finalDamage = damage;
            } else {
              await redisSet(`${playerId}`, "hp", `${playerHp - 1}`);
              finalDamage = 1;
            }
          } else {
            currentSkill = "Miss";
            await redisSet(`${playerId}`, "hp", `${playerHp - 1}`);
            finalDamage = 1;
          }
        }
    }

    const playerAtk = await redisGet(`${playerId}`, "atk");
    const latestPlayerHp = await redisGet(`${playerId}`, "hp");
    const latestPlayerMp = await redisGet(`${playerId}`, "mp");
    const latestMonsterHp = await redisGet(`${playerId}_${monsterId}`, "hp");

    if (latestPlayerHp <= 0) {
      await this.battleService.death(playerId);
      return res.json([
        {
          alive: false,
          hp: latestPlayerHp,
          mp: latestPlayerMp,
          atk: playerAtk,
          id: playerId,
        },
        { hp: latestMonsterHp, atk: monsterAtk, id: monsterId },
        { usingSkill: currentSkill, damageDone: finalDamage },
      ]);
    }

    return res.json([
      {
        alive: true,
        hp: latestPlayerHp,
        mp: latestPlayerMp,
        atk: playerAtk,
        id: playerId,
      },
      { hp: latestMonsterHp, atk: monsterAtk, id: monsterId },
      { usingSkill: currentSkill, damageDone: finalDamage },
    ]);
  };

  escapeFunction = async (req: Request, res: Response) => {
    const playerId = req.session?.user.id;
    const monsterId = req.body.monsterId;
    const playerHp = req.body.playerHp;
    const playerMp = req.body.playerMp;

    await this.battleService.escapeFunction(
      playerId,
      playerHp,
      playerMp,
      monsterId
    );

    return res.json({ result: "Escaped" });
  };

  battleUseItem = async (req: Request, res: Response) => {
    const playerId = req.session?.user.id;
    const monsterId = req.body.monsterId;
    const itemId = req.body.itemId;
    const redisGet = util.promisify(this.client.hget).bind(this.client);
    const redisSet = util.promisify(this.client.hset).bind(this.client);

    const playerHp = +(await redisGet(`${playerId}`, "hp"));
    const playerMp = +(await redisGet(`${playerId}`, "mp"));
    const playerAtk = +(await redisGet(`${playerId}`, "atk"));
    const monsterAtk = +(await redisGet(`${playerId}_${monsterId}`, "atk"));

    await this.battleService.battleUseItem(playerId, itemId);

    const changes = allitemsList[itemId].atk;

    if (itemId === 8) {
      await redisSet(`${playerId}`, "mp", `${playerMp + changes}`);
    } else if (itemId === 9) {
      await redisSet(`${playerId}`, "hp", `${playerHp + changes}`);
    }

    const latestPlayerHp = await redisGet(`${playerId}`, "hp");
    const latestPlayerMp = await redisGet(`${playerId}`, "mp");
    const latestMonsterHp = await redisGet(`${playerId}_${monsterId}`, "hp");

    return res.json([
      { hp: latestPlayerHp, mp: latestPlayerMp, atk: playerAtk, id: playerId },
      { hp: latestMonsterHp, atk: monsterAtk, id: monsterId },
    ]);
  };
}
