import { Request, Response } from 'express'
import { checkPassword } from '../hash'


import { PlayerService } from '../service/PlayerService'



export class PlayerController {
    constructor(private playerService: PlayerService) { }


    signUp = async (req: Request, res: Response) => {
        const result = (await this.playerService.signUp(req.body))

        if (result[0]) {
            if (req.session) {
                req.session.user = {
                    id: result[1][0]
                }

            }
            return res.redirect(`/select.html?id=${req.session?.user.id}`)
        } else {
            res.json({ signup: false })
        }
    }


    logIn = async (req: Request, res: Response) => {
        const { userName, password } = req.body
        const result = (await this.playerService.logIn(userName))


        const match = await checkPassword(password, result[0].password)

        if (match) {
            if (req.session) {
                req.session.user = {
                    id: result[0].id
                };
            }
            return res.redirect("/face-detection.html")
        } else {
            return res.json({ login: false })
        }
    }

    faceDetection = async (req: Request, res: Response) => {
        const {verified} = req.body;
        if (verified) {
            req.session!.user.verified = true;
            return res.redirect(`/select.html?id=${req.session?.user.id}`)
        } else {
            return res.redirect(`/face-detection.html`)
        }
    }

    select = async (req: Request, res: Response) => {
        const playerId = req.session?.user.id
        const occId = req.body.occId
        const body = [playerId, occId]


        await this.playerService.select(body)
        await this.playerService.MonsterSave(playerId,1)


        return res.json({ account: true })

    }
    logout = async (req: Request, res: Response) => {
        if (req.session) {
            delete req.session.user;
        }
        res.redirect('/index.html');

    }

    saveData = async (req: Request, res: Response) => {
        const playerId = req.session?.user.id
        const location = req.body.location
        const skills = req.body.skills


        await this.playerService.saveData(playerId,location,skills)
        return res.json({save:true})
    }
}