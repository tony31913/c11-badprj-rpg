import { Request, Response } from "express";
import { SkillService } from "../service/SkillService";
import { allSkillList } from "../skill";
import * as occ from "../occupation";
import redis from "redis";
import util from "util";

export class SkillController {
  constructor(
    public skillService: SkillService,
    public client: redis.RedisClient
  ) {}

  loadSkill = async (req: Request, res: Response) => {
    const playerId = req.session?.user.id;
    const result = await this.skillService.loadSkill(playerId);

    let skills: object[] = [];
    const skill1 = result[0].skill_1;
    const skill2 = result[0].skill_2;
    const skill3 = result[0].skill_3;
    const skill4 = result[0].skill_4;

    skills.push(allSkillList[skill1]);
    skills.push(allSkillList[skill2]);
    skills.push(allSkillList[skill3]);
    skills.push(allSkillList[skill4]);

    return res.json(skills);
  };

  loadAllSkills = async (req: Request, res: Response) => {
    return res.json(allSkillList);
  };

  useSkill = async (req: Request, res: Response) => {
    const playerId = req.session?.user.id;
    const monsterId = req.body.monsterId;
    const skillId = req.body.skillId;

    const data = await this.skillService.checkCurrentLevel(playerId);
    const playerLevel = data[0].level;
    const playerOcc = data[0].occupation_id;
    let playerMaxHp;
    switch (playerOcc) {
      case 1:
        playerMaxHp = (playerLevel - 1) * occ.warrior.hpAdd + occ.warrior.hp;
        break;
      case 2:
        playerMaxHp = (playerLevel - 1) * occ.mage.hpAdd + occ.mage.hp;
        break;
      case 3:
        playerMaxHp = (playerLevel - 1) * occ.hunter.hpAdd + occ.hunter.hp;
        break;
    }

    const redisGet = util.promisify(this.client.hget).bind(this.client);
    const redisSet = util.promisify(this.client.hset).bind(this.client);

    const playerAtk = +(await redisGet(`${playerId}`, "atk"));
    const playerHp = +(await redisGet(`${playerId}`, "hp"));
    const playerMp = +(await redisGet(`${playerId}`, "mp"));
    // const playerDef = +await redisGet(`${playerId}`, 'def')

    const monsterAtk = +(await redisGet(`${playerId}_${monsterId}`, "atk"));
    const monsterHp = +(await redisGet(`${playerId}_${monsterId}`, "hp"));
    const monsterDef = +(await redisGet(`${playerId}_${monsterId}`, "def"));

    const skill = allSkillList[skillId];

    if (playerMp >= skill.mp) {
      await redisSet(`${playerId}`, "mp", `${playerMp - skill.mp}`);
      if (!skill.is_healing) {
        const damage = Math.round(
          (playerAtk + skill.dmg) * skill.dmgR - monsterDef
        );

        if (damage > 0) {
          await redisSet(
            `${playerId}_${monsterId}`,
            "hp",
            `${monsterHp - damage}`
          );
        }
      } else {
        //set maxHP for the player
        const afterHeal = playerHp + skill.heal;
        if (afterHeal < playerMaxHp) {
          await redisSet(`${playerId}`, "mp", `${playerMp - skill.mp}`);
          await redisSet(`${playerId}`, "hp", `${playerHp + skill.heal}`);
        } else {
          await redisSet(`${playerId}`, "mp", `${playerMp - skill.mp}`);
          await redisSet(`${playerId}`, "hp", `${playerMaxHp}`);
        }
      }
    } else {
      return res.json({ Skill: false });
    }

    const latestPlayerHp = await redisGet(`${playerId}`, "hp");
    const latestPlayerMp = await redisGet(`${playerId}`, "mp");
    const latestMonsterHp = await redisGet(`${playerId}_${monsterId}`, "hp");

    if (latestMonsterHp <= 0) {
      const result = await this.updateAfterWin(
        playerId,
        latestPlayerHp,
        latestPlayerMp,
        playerAtk,
        monsterId
      );

      return res.json(result);
    } else {
      return res.json([
        {
          hp: latestPlayerHp,
          mp: latestPlayerMp,
          atk: playerAtk,
          id: playerId,
        },
        { hp: latestMonsterHp, atk: monsterAtk, id: monsterId },
      ]);
    }
  };

  updateAfterWin = async (
    playerId: number,
    latestPlayerHp: number,
    latestPlayerMp: number,
    playerAtk: number,
    monsterId: number
  ) => {
    const data = await this.skillService.saveBattle(
      playerId,
      latestPlayerHp,
      latestPlayerMp,
      monsterId
    );
    const result = await this.skillService.checkLevel(playerId);

    return [
      {
        hp: latestPlayerHp,
        mp: latestPlayerMp,
        id: playerId,
        location: data[0].location,
        expPoint: data[0].expPoint,
        pastLevel: data[1][0],
        newLevel: result,
        atk: playerAtk,
      },
      { hp: 0, id: monsterId, alive: false },
    ];
  };
}
