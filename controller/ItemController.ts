import { ItemService } from "../service/item.Service";
import { Request, Response } from "express";

export class ItemController {
  constructor(private itemService: ItemService) {}

  loadCharItems = async (req: Request, res: Response) => {
    const playerId = req.session?.user.id;
    const data = await this.itemService.loadCharItems(playerId);
    return res.json(data);
  };

  loadItemsList = async (req: Request, res: Response) => {
    const data = await this.itemService.loadItemsList();
    return res.json(data);
  };

  checkCharCurrency = async (req: Request, res: Response) => {
    const charId = req.session?.user.id;
    const data = await this.itemService.checkCharCurrency(charId);
    return res.json(data);
  };

  buyItems = async (req: Request, res: Response) => {
    const charId = req.session?.user.id;
    const itemId = req.body.itemId;
    const charGold = await this.itemService.checkCharCurrency(charId);
    const itemGold = await this.itemService.loadItemGold(itemId);

    const goldAfterBuying = charGold - itemGold;

    if (goldAfterBuying >= 0) {
      await this.itemService.buyItems(itemId, charId);
      await this.itemService.updateCharCurrency(charId, goldAfterBuying);
      return res.json({ outcome: "ok", gold: goldAfterBuying });
    }

    return res.json({ outcome: "failed", gold: charGold });
    //     const data = await this.itemService.buyItems(itemId ,charId)
    //     return res.json(data)
    // }
  };

  updateEquippedStatus = async (req: Request, res: Response) => {
    const charId = req.session?.user.id;
    const itemId = req.body.itemId;
    const status = req.body.equipped;
    await this.itemService.updateEquippedStatus(charId, itemId, status);

    return res.json("ok");
  };

  usePotion = async (req: Request, res: Response) => {
    const charId = req.session?.user.id;
    const itemId = req.body.itemId;
    await this.itemService.usePotion(charId, itemId);
    const data = await this.itemService.selectedPotionValue(itemId);
    return res.json(data);
  };

  updateHpMp = async (req: Request, res: Response) => {
    const charId = req.session?.user.id;
    const newHp = req.body.hp;
    const newMp = req.body.mp;
    await this.itemService.updateHpMp(charId, newHp, newMp);
    return res.json("ok");
  };
}
