interface Skill {
    id: number;
    name: string;
    mp: number;
    is_healing: boolean;
    dmg: number
    dmgR: number;
    heal: number;
    image_index: string
}

export const allSkillList: Skill[] = [
    {
        id: 0,
        name: "this is a dummy",
        mp: 0,
        is_healing: false,
        dmg: 0,
        dmgR: 0,
        heal: 0,
        image_index: '[0,0]'
    },

    {
        id: 1,
        name: "Heavy Attack",
        mp: 20,
        is_healing: false,
        dmg: 0,
        dmgR: 2,
        heal: 0,
        image_index: '[1,3]'
    },

    {
        id: 2,
        name: "Blessing",
        mp: 50,
        is_healing: true,
        dmg: 0,
        dmgR: 0,
        heal: 20,
        image_index: '[5,3]'
    },



    {
        id: 3,
        name: "Fireball",
        mp: 20,
        is_healing: false,
        dmg: 100,
        dmgR: 1,
        heal: 0,
        image_index: '[9,0]'
    },

    {
        id: 4,
        name: 'Rest',
        mp: 0,
        is_healing: true,
        dmg: 0,
        dmgR: 0,
        heal: 5,
        image_index: "[7,0]"
    },
    {
        id: 5,
        name: "Silver Bolts",
        mp: 20,
        is_healing: false,
        dmg: 40,
        dmgR: 1,
        heal: 0,
        image_index: "[4,3]"
    },
    {
        id: 6,
        name: "Storm",
        mp: 100,
        is_healing: false,
        dmg: 200,
        dmgR: 2.25,
        heal: 0,
        image_index: "[8,0]"
    }, {
        id: 7,
        name: "First-aid",
        mp: 20,
        is_healing: true,
        dmg: 0,
        dmgR: 0,
        heal: 50,
        image_index: "[6,3]"
    },
    {
        id: 8,
        name: "Crippling Strike",
        mp: 50,
        is_healing: false,
        dmg: 70,
        dmgR: 1,
        heal: 0,
        image_index: "[0,3]"
    }];

