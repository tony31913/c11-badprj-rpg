import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("monsters_list").del();

    // Inserts seed entries
    await knex("monsters_list").insert([
        { id: 1, name: "Skeleton", hp: 50, atk: 5, alive: true, location: '[5,3]', img: './assets/skeleton.png', expPoint: 10, gold: 10, def: 2 },
        { id: 2, name: "Dark Skeleton", hp: 100, atk: 12, alive: true, location: '[5,6]', img: './assets/dark_skeleton.png', expPoint: 30, gold: 20, def: 4 },
        { id: 3, name: "Ghost", hp: 40, atk: 8, alive: true, location: '[4,10]', img: './assets/ghost.png', expPoint: 20, gold: 10, def: 1 },
        { id: 10, name:"Boss", hp:400, atk:15, alive:true, location:'[8,5]',img:'./assets/mini_boss.png',expPoint:150,gold:100,def:7}
    ]);
};
