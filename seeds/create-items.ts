import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("items").del();

    // Inserts seed entries
    await knex("items").insert([
        { id: 1, name: "Wooden Sword", def: 0, atk: 10, gold: 10, type: "Weapon", image_index: "[0, 5]" },
        { id: 2, name: "Wooden Armor", def: 10, atk: 0, gold: 10, type: "Clothes", image_index: "[6, 7]" },
        { id: 3, name: "Iron Armor", def: 15, atk: 0, gold: 10, type: "Clothes", image_index: "[7, 7]" },
        { id: 4, name: "Iron Axe", def: 0, atk: 12, gold: 10, type: "Weapon", image_index: "[1, 10]" },
        { id: 5, name: "Arrow", def: 0, atk: 10, gold: 5, type: "Weapon", image_index: "[3, 6]" },
        { id: 6, name: "Iron Sword", def: 0, atk: 15, gold: 10, type: "Weapon", image_index: "[1, 5]" },
        { id: 7, name: "Only Shoes", def: 10, atk: 0, gold: 20, type: "Shoes", image_index: "[2, 8]" },
        { id: 8, name: "Magic Potion", def: 0, atk: 10, gold: 5, type: "Potion", image_index: "[2, 9]", consumable: true },
        { id: 9, name: "Health Potion", def: 0, atk: 10, gold: 5, type: "Potion", image_index: "[0, 9]", consumable: true }
    ]);
};
