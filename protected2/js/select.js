
// clicking events
document.querySelector("#warrior")
    .addEventListener('click', async () => {
       await fetch('/select', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                "occId": 1
              })
    })
    location.reload();
    
})
    

document.querySelector("#mage")
    .addEventListener('click', async () => {
        await fetch('/select', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                "occId": 2
              })
    })
    location.reload();
})



document.querySelector("#hunter")
    .addEventListener('click', async () => {
        await fetch('/select', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                "occId": 3
              })
    })
    location.reload();
})



//hover events
document.querySelector('#warrior')
.addEventListener('mouseover',()=>{
  document.querySelector('#warrior_detail').innerHTML = `
  <div> Warrior is a class with high def.</div> 
  `
})

document.querySelector('#warrior')
.addEventListener('mouseout',()=>{
  document.querySelector('#warrior_detail').innerHTML = ` `
})


document.querySelector('#mage')
.addEventListener('mouseover',()=>{
  document.querySelector('#mega_detail').innerHTML = `
  <div> Mega is super glass cannon.</div> 
  `
})

document.querySelector('#mage')
.addEventListener('mouseout',()=>{
  document.querySelector('#mega_detail').innerHTML = ` `
})

document.querySelector('#hunter')
.addEventListener('mouseover',()=>{
  document.querySelector('#hunter_detail').innerHTML = `
  <div> Hunter is an average class.</div> 
  `
})

document.querySelector('#hunter')
.addEventListener('mouseout',()=>{
  document.querySelector('#hunter_detail').innerHTML = ` `
})