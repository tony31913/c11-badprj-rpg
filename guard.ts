import {Request,Response,NextFunction} from 'express';
import {knex} from './main'

export function isLoggedIn(req:Request,res:Response,next:NextFunction){
    if(req.session?.user){
        next();
    }else{
        res.redirect('/index.html');
    }
}

export async function alreadyCreated(req:Request, res:Response, next:NextFunction){
    const playerId = req.session?.user.id
    const result =await knex('characters')
    .select('*')
    .where('player_id',playerId)

    if(result[0] == null || result[0].alive == false){
        next();
    }else{
        res.redirect(`/game.html?id=${req.session?.user.id}`)
    }

}

export async function passedFaceDetection (req: Request, res: Response, next: NextFunction) {
    const verified = req.session?.user.verified;
    if (verified) {
        next();
    } else {
        res.redirect("/face-detection.html")
    }
}