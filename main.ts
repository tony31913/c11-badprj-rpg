//package import
import express from "express";
import dotenv from "dotenv";
import * as bodyParser from "body-parser";
import expressSession from "express-session";
import Knex from "knex";
import redis from "redis";

//redis connect
export const client = redis.createClient();

//database connect
dotenv.config();
const knexConfig = require("./knexfile");
export const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

const app = express();

//express handler routes
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(
  expressSession({
    secret: "rpgProject",
    resave: true,
    saveUninitialized: true,
  })
);

//Services
import { PlayerService } from "./service/PlayerService";
const playerService = new PlayerService(knex);
import { BattleService } from "./service/BattleService";
const battleService = new BattleService(knex);
import { ItemService } from "./service/item.Service";
const itemService = new ItemService(knex);
import { SkillService } from "./service/SkillService";
const skillService = new SkillService(knex);

//Controller
import { PlayerController } from "./controller/PlayerController";
export const playerController = new PlayerController(playerService);
import { BattleController } from "./controller/BattleController";
export const battleController = new BattleController(battleService, client);
import { ItemController } from "./controller/ItemController";
export const itemController = new ItemController(itemService);
import { SkillController } from "./controller/SkillController";
export const skillController = new SkillController(skillService, client);

// test area

//router
import { routes } from "./router";
app.use("/", routes);
app.use(express.static("public"));
import { isLoggedIn, alreadyCreated, passedFaceDetection } from "./guard";
app.use(isLoggedIn, express.static("protected"));
app.use(passedFaceDetection);
app.use(alreadyCreated, express.static("protected2"));

//Port
const PORT = 8080;

app.listen(PORT, () => {
  console.log(`Listening at http://localhost:${PORT}/`);
});
