### Using google colab to train the model

1. Upload the dataset folder to the root of google drive.
2. Change runtime type of colab to GPU.
3. Run all code of facial_emotion.ipynb.
