import { PlayerController } from '../controller/PlayerController';
import { PlayerService } from '../service/PlayerService';
import { Request, Response } from 'express';
import Knex from 'knex';
import { checkPassword } from '../hash'
jest.mock('express');
jest.mock('../hash');

describe("PlayerController", () => {

    let controller: PlayerController;
    let service: PlayerService;
    let req: Request;
    let res: Response;

    beforeEach(function () {
        service = new PlayerService({} as Knex);
        controller = new PlayerController(service);
        req = {
            body: {},
            params: {},
            session: {
                user: {
                    id: 1
                }
            }
        } as any as Request;

        res = {
            json: jest.fn(),
            redirect: jest.fn()
        } as any as Response;

    })

    it("should handle valid sign up correctly", async () => {

        req.body = {
            userName: "tony",
            displayName: "tony",
            password: "123456"
        }

        jest.spyOn(service, "signUp").mockImplementation(async (body) => [true, [1]]);

        await controller.signUp(req, res);
        expect(service.signUp).toBeCalledTimes(1);
        expect(service.signUp).toBeCalledWith(req.body);
        expect(res.redirect).toBeCalledWith("/select.html?id=1");
    })

    it("should reject the user to sign up", async () => {
        req.body = {
            userName: "tony",
            displayName: "tony",
            password: "123456"
        }

        jest.spyOn(service, "signUp").mockImplementation(async (body) => [false, "UserName in used"]);

        await controller.signUp(req, res);
        expect(service.signUp).toBeCalledTimes(1);
        expect(res.json).toBeCalledWith({ signup: false });
    })

    it("should handle login method correctly", async () => {
        req.body = {
            userName: "tony",
            password: "123456"
        }

        jest.spyOn(service, "logIn").mockImplementation(async (body) => [{ id: 1, username: "tony", display_name: "tony", password: "123456" }]);

        (checkPassword as jest.Mock).mockReturnValue(true);

        await controller.logIn(req, res);
        expect(service.logIn).toBeCalledTimes(1);
        expect(checkPassword).toBeCalledTimes(1);
        expect(res.redirect).toBeCalledWith("/select.html?id=1");
    })

    it("should reject the user to log in", async () => {

        req.body = {
            userName: "tony",
            password: "123456"
        }

        jest.spyOn(service, "logIn").mockImplementation(async (body) => [{ id: 1, username: "tony", display_name: "tony", password: "123456" }]);

        (checkPassword as jest.Mock).mockReturnValue(false);

        await controller.logIn(req, res);
        expect(service.logIn).toBeCalledTimes(1);
        expect(res.json).toBeCalledWith({ login: false });
    })

    it("should handled select occupation correctly", async () => {
        req.body = {
            playerId: 1,
            occId: 1
        }

        jest.spyOn(service, "select").mockImplementation((body) => Promise.resolve({ success: true }));
        jest.spyOn(service, "MonsterSave").mockImplementation();

        await controller.select(req, res);
        expect(service.select).toBeCalledTimes(1);
        expect(service.MonsterSave).toBeCalledTimes(1);
        expect(res.json).toBeCalledWith({ account: true });
    })
})