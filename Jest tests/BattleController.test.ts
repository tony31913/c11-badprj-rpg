import { BattleController } from "../controller/BattleController";
import { BattleService } from "../service/BattleService";
import { Request, Response } from "express";
import { RedisClient } from "redis";
import Knex from "knex";
jest.mock("express");

describe("BattleController", () => {
  let controller: BattleController;
  let service: BattleService;
  let client: RedisClient;
  let req: Request;
  let res: Response;

  beforeEach(function () {
    service = new BattleService({} as Knex);
    client = ({
      hset: jest.fn((id, key, value, cb) => cb(null)),
    } as any) as RedisClient;

    jest.spyOn(service, "loadData").mockImplementation(async () => [
      {
        id: 1,
        level: 9,
        exp: 140,
        hp: 19,
        atk: 10,
        def: 4,
        location: "[3,1]",
        img: ".assets/warrior02.png",
        occupation_id: 1,
        player_id: 1,
        alive: "t",
      },
    ]);

    jest.spyOn(service, "loadEquippedItems").mockImplementation(async () => {
      return [0, 0];
    });

    controller = new BattleController(service, client);

    req = ({
      session: {
        user: {
          id: 1,
        },
      },
    } as any) as Request;

    res = ({
      json: jest.fn(),
    } as any) as Response;
  });

  it("should handle loadData method correctly", async () => {
    let hsetSpy = jest.spyOn(controller.client, "hset");
    await controller.loadData(req, res);
    expect(service.loadData).toBeCalledTimes(1);
    expect(client.hset).toBeCalledTimes(4);
    expect(hsetSpy.mock.calls[0][0]).toBe("1");
    expect(hsetSpy.mock.calls[1][0]).toBe("1");
    expect(hsetSpy.mock.calls[2][0]).toBe("1");
    expect(res.json).toBeCalledWith([
      {
        id: 1,
        level: 9,
        exp: 140,
        hp: 19,
        atk: 10,
        def: 4,
        location: "[3,1]",
        img: ".assets/warrior02.png",
        occupation_id: 1,
        player_id: 1,
        alive: "t",
      },
    ]);
  });
});
