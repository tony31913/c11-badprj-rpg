document.querySelector('#signUp').addEventListener('click', () => {
    document.querySelector('#main').innerHTML = `
    <div class="box_area">
    <h3>Sign Up</h3>
    <form action="/account" method="post">
            <div class="form-group">
                <input name="userName" type="text" placeholder="Usesrname">
                <span class="error_message" name ="errorMsg"></span>
            </div>
            <div class="form-group">
                <input name="displayName" type="text" placeholder="Display Name">
            </div>
            <div class="form-group">
                <input name="password" type="password" placeholder="Password">
            </div>
            <div class="form-group">
                <input name="password2" type="password" placeholder="Confirm Password">
        </div>
            <button type="submit" class="btn btn-primary sign-in-btn">Sign up</button>
    </form>
        <span class="back acc_button">back</span>
    </div>
     `
    document.querySelector(".back").addEventListener('click', () => {
        location.reload()
    })
})



document.querySelector('#logIn').addEventListener('click', () => {
    document.querySelector('#main').innerHTML = `
    <div class="box_area">
    <h3>Login</h3>
    <form action="/login" method="post">
        <div class="form-group">
            <input name="userName" type="text" placeholder="Username">
            <span class="error_message" name ="errorMsg"></span>
        </div>
        <div class="form-group">
            <input name="password" type="password" placeholder="Password">
        </div>
        <button type="submit" class="btn btn-primary">Log In</button>
    </form>
    <span class="back acc_button">back</span>
    </div>
    `
    document.querySelector(".back").addEventListener('click', () => {
        location.reload()
    })
})


