function resizeCanvasForMap() {

    myCanvas.width = window.innerWidth > 1000 ? 1000 : window.innerWidth;
    myCanvas.height = window.innerHeight - 96;
    if (myCanvas.height > myCanvas.width) {
        myCanvas.height = myCanvas.width;
    }
    viewPoint.canvasSize = [myCanvas.width, myCanvas.height];

    myCanvas.style.width = myCanvas.width + "px";
    myCanvas.style.height = myCanvas.height + "px";
}

function resizeCanvasForBattleAndPanel() {
    myCanvas.width = 1000;
    myCanvas.height = 1000;

    let adjustedCanvasWidth = window.innerWidth > 1000 ? 1000 : window.innerWidth;
    let adjustedCanvasHeight = window.innerHeight - 96;

    if (adjustedCanvasWidth > adjustedCanvasHeight) {
        adjustedCanvasWidth = adjustedCanvasHeight;
    } else {
        adjustedCanvasHeight = adjustedCanvasWidth;
    }

    myCanvas.style.width = adjustedCanvasWidth + "px";
    myCanvas.style.height = adjustedCanvasHeight + "px";
}