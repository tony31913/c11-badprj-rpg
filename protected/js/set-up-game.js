const tileLength = 40;
const spriteSize = 32;
const columns = 24;
const rows = 24;

let allMonster = []
let allSkills = []
let player;


const myCanvas = document.querySelector("#my-canvas");
const ctx = myCanvas.getContext("2d");

const buttonContainer = document.querySelector("#buttons-container");

const gameMap = [
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0,
    0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0,
    0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0,
    0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0,
    0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 0,
    0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0,
    0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0,
    0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
    0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0,
    0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0,
    0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0,
    0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
    0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
    0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0,
    0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0,
    0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0,
    0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
];

let gameMode = "map";

let keysDown = {
    ArrowLeft: false,
    ArrowUp: false,
    ArrowRight: false,
    ArrowDown: false,
    Space: false,
    Escape: false
};

let viewPoint = {
    canvasSize: [myCanvas.width, myCanvas.height],
    startTile: [0, 0],
    endTile: [0, 0],
    offset: [0, 0], // It is the part beyond our map's boundary
    update: function (centerX, centerY) {
        this.offset[0] = Math.floor((this.canvasSize[0] / 2) - centerX);
        this.offset[1] = Math.floor((this.canvasSize[1] / 2) - centerY);

        let centerTile = [Math.floor(centerX / tileLength), Math.floor(centerY / tileLength)];

        this.startTile[0] = centerTile[0] - Math.ceil((this.canvasSize[0] / 2) / tileLength);
        this.startTile[1] = centerTile[1] - Math.ceil((this.canvasSize[1] / 2) / tileLength);

        if (this.startTile[0] < 0) {
            this.startTile[0] = 0;
        }

        if (this.startTile[1] < 0) {
            this.startTile[1] = 0;
        }

        this.endTile[0] = centerTile[0] + Math.ceil((this.canvasSize[0] / 2) / tileLength);
        this.endTile[1] = centerTile[1] + Math.ceil((this.canvasSize[1] / 2) / tileLength);

        if (this.endTile[0] >= columns) {
            this.endTile[0] = columns - 1;
        }

        if (this.endTile[1] >= rows) {
            this.endTile[1] = rows - 1;
        }
    }
};

let battleSoundtrack = new Audio("./assets/soundtrack/The \Arrival \(BATTLE II).mp3");
battleSoundtrack.loop = true;

let mapSoundtrack = new Audio("./assets/soundtrack/Nocturnal \Mysteries.mp3");
mapSoundtrack.loop = true;

let panelSoundtrack = new Audio("./assets/soundtrack/Windless \Slopes.mp3");
panelSoundtrack.loop = true;

let gameOverSoundTrack = new Audio("./assets/soundtrack/Foggy \Woods.mp3");
gameOverSoundTrack.loop = true;

let battleActions = [{ name: "Attack" }, { name: "Skills" }, { name: "Items" }, { name: "Escape" }];
let skillOptions = [];
let itemList = [];

//For battle Display
let usedSkill;
let healthChange = 0;
//
let skillEquiped = [];
let charItems = [];

let battlePhrase = "NA";
let phraseFrameCount = 0;
let battleOptionsArray = [];
let currentMonster = null

let panelShowingInfo = "NA";
let panelSubActionIndex = 0;
let musicMuted = 0;

let tileSheet = new Image();
tileSheet.src = "./assets/tilesheet.png";

let tileSheet2 = new Image();
tileSheet2.src = "./assets/tileset.png";

let itemImage = new Image();
itemImage.src = "./assets/itemsheet.png"

let gameOverImage = new Image();
gameOverImage.src = "./assets/grave.png";


window.onload = async function () {

    await loadAllMonster();
    await loadAllSkills();
    await loadPlayer();
    await loadSkill();
    await loadCharItems();
    resizeCanvasForMap();

    if (window.innerWidth >= window.innerHeight) {
        buttonContainer.innerHTML = "";
    } else {
        buttonContainer.innerHTML = `
        <button type="button" class="btn btn-dark cancel-button">Cancel</button>
        <button type="button" class="btn btn-dark confirm-button">Confirm</button>
        <div id="arrow-buttons-container">
          <div id="arrow-up-container">
            <div class="icon-bubble arrow-up-button">
              <i class="fas fa-arrow-up"></i>
            </div>
          </div>
          <div id="arrow-left-right-container">
            <div class="icon-bubble arrow-left-button">
              <i class="fas fa-arrow-left"></i>
            </div>
            <div class="icon-bubble arrow-right-button">
              <i class="fas fa-arrow-right"></i>
            </div>
          </div>
          <div id="arrow-down-container">
            <div class="icon-bubble arrow-down-button">
              <i class="fas fa-arrow-down"></i>
            </div>
          </div>
        `
    }

    window.addEventListener("keydown", function (event) {
        if (Object.keys(keysDown).includes(event.code)) {
            keysDown[event.code] = true;
        }
    });
    window.addEventListener("keyup", function (event) {
        if (Object.keys(keysDown).includes(event.code)) {
            keysDown[event.code] = false;
        }
    });

    window.addEventListener("resize", () => {
        if (gameMode == "map") {
            resizeCanvasForMap();
        } else {
            resizeCanvasForBattleAndPanel();
        }

        if (window.innerWidth >= window.innerHeight) {
            buttonContainer.innerHTML = "";
        } else {
            buttonContainer.innerHTML = `
            <button type="button" class="btn btn-dark cancel-button">Cancel</button>
            <button type="button" class="btn btn-dark confirm-button">Confirm</button>
            <div id="arrow-buttons-container">
              <div id="arrow-up-container">
                <div class="icon-bubble arrow-up-button">
                  <i class="fas fa-arrow-up"></i>
                </div>
              </div>
              <div id="arrow-left-right-container">
                <div class="icon-bubble arrow-left-button">
                  <i class="fas fa-arrow-left"></i>
                </div>
                <div class="icon-bubble arrow-right-button">
                  <i class="fas fa-arrow-right"></i>
                </div>
              </div>
              <div id="arrow-down-container">
                <div class="icon-bubble arrow-down-button">
                  <i class="fas fa-arrow-down"></i>
                </div>
              </div>
            `
        }
    }
    )

    window.addEventListener("beforeunload", async () => {
        await saveData(player.tileFrom);
    })

    buttonContainer.addEventListener("touchstart", (event) => {
        if (event.target.matches(".arrow-left-button")) {
            keysDown["ArrowLeft"] = true;
        } else if (event.target.matches(".arrow-up-button")) {
            keysDown["ArrowUp"] = true;
        } else if (event.target.matches(".arrow-right-button")) {
            keysDown["ArrowRight"] = true;
        } else if (event.target.matches(".arrow-down-button")) {
            keysDown["ArrowDown"] = true;
        } else if (event.target.matches(".confirm-button")) {
            keysDown["Space"] = true;
        } else if (event.target.matches(".cancel-button")) {
            keysDown["Escape"] = true;
        }
    })

    buttonContainer.addEventListener("touchend", (event) => {
        event.preventDefault();
        if (event.target.matches(".arrow-left-button")) {
            keysDown["ArrowLeft"] = false;
        } else if (event.target.matches(".arrow-up-button")) {
            keysDown["ArrowUp"] = false;
        } else if (event.target.matches(".arrow-right-button")) {
            keysDown["ArrowRight"] = false;
        } else if (event.target.matches(".arrow-down-button")) {
            keysDown["ArrowDown"] = false;
        } else if (event.target.matches(".confirm-button")) {
            keysDown["Space"] = false;
        } else if (event.target.matches(".cancel-button")) {
            keysDown["Escape"] = false;
        }
    })

    mapSoundtrack.play();
    await requestAnimationFramePromise(drawGame);
};



async function drawGame() {

    if (gameMode == "map") {
        await drawMap();
    } else if (gameMode == "battle") {
        await drawBattle();
    } else if (gameMode == "panel") {
        await drawPanel();
    } else {
        drawGameOver();
    }

    await requestAnimationFramePromise(drawGame);
}

function lastActionIsFinished() {
    if ((Date.now() - player.lastActionTime) > player.timeNeededForAnAction) {
        return true;
    } else {
        return false;
    }
}

function updateLastActionTime() {
    player.lastActionTime = Date.now();
}

//maybe should return the result   can be written better here
async function loadPlayer() {
    const res = await fetch('loadData', {
        method: 'get'
    })
    const result = await res.json()



    const dummy = new Character(JSON.parse(result[0].location), JSON.parse(result[0].location), result[0].img)
    player = dummy

    player.updateData(result);
}

async function loadSkill() {
    const res = await fetch('/loadSkill', {
        method: 'get'
    })
    const result = await res.json()


    skillEquiped = []
    for (let i = 0; i < result.length; i++) {
        skillEquiped.push(result[i])
    }


}

async function loadAllSkills() {
    const res = await fetch('/loadAllskills', {
        method: 'get'
    })
    const result = await res.json()


    for (let skill of result) {
        allSkills.push(skill)
    }

}

async function getShopItems() {
    const res = await fetch('loadShopItems', {
        method: 'get'
    })
    const result = await res.json()
    itemList = []
    for (let i = 0; i < result.length; i++) {
        itemList.push(result[i])
    }
}

//actually we should return result in this function  than outside the function run the for loop ?????
async function loadAllMonster() {
    const res = await fetch('/loadAllMonster', {
        method: 'get'
    })

    const result = await res.json()

    for (let monster of result) {

        const newMonster = new Enemy(monster.id, JSON.parse(monster.location), JSON.parse(monster.location), monster.img, monster.alive);

        allMonster.push(newMonster)
    }
}

function requestAnimationFramePromise(callback) {
    return new Promise((resolve) => {
        requestAnimationFrame(callback)
        resolve();
    })
}

// test item function
async function loadCharItems() {
    const res = await fetch('loadCharItems', {
        method: 'get'
    })
    const result = await res.json()

    charItems = []
    for (let i = 0; i < result.length; i++) {
        charItems.push(result[i]);
    }
    player.items = charItems
    return
}

var purchaseResult;
async function buyItems(itemId) {
    const res = await fetch('buyItems', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "itemId": itemId
        })
    })
    const result = await res.json()
    return result
}

async function checkPurchaseResult() {
    //  select * from "char-items" inner join items on "char-items".item_id = items.id;
}

async function checkCharGold() {
    const res = await fetch('checkCharGold', {
        method: 'get'
    })
    const result = await res.json()
    player.updateGold(result)
    return
}

async function updateEquippedStatus(itemId, status) {
    await fetch('updateEquippedStatus', {
        method: 'put',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "itemId": itemId,
            "equipped": status
        })
    })
}

async function useItem(itemId) {
    const res = await fetch('useItems', {
        method: 'put',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "itemId": itemId,
        })
    })
    let result = await res.json()
    if (result[0].name === 'Health Potion') {
        player.updateHP(result)
    } else {
        player.updateMP(result)
    }
}

async function updateHpMp(newHp, newMp) {
    await fetch('updateHpMp', {
        method: 'put',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "hp": newHp,
            "mp": newMp
        })
    })
}


