alert('To prevent cheating, we need to ensure you are a human being instead of a robot. Please allow us to turn on your camera, and smile in front of the camera to pass this test.')

const video = document.getElementById('video');

function startVideo() {
  if (navigator.mediaDevices.getUserMedia) {
    navigator.mediaDevices.getUserMedia({
      video: {
        width: 224,
        height: 224
      }
    })
      .then(function (stream) {
        video.srcObject = stream;
      })
      .catch(function (err) {
        console.log("err: ", err);
      });
  }
}

video.addEventListener('playing', () => {
  let myInterval = setInterval(async () => {
    try {
      const canvas = document.getElementById('canvas');
      const ctx = canvas.getContext('2d');
      ctx.drawImage(video, 0, 0, 224, 224);

      canvas.toBlob(async (blob) => {
        const formData = new FormData();

        formData.append('image', blob);

        const res = await fetch("http://localhost:8888", {
          method: 'post',
          body: formData,
        })

        const json = await res.json();
        console.log('res: ', json);

        if (json.data === "happy") {
          clearInterval(myInterval);
          await fetch('/faceDetection', {
            method: 'post',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              "verified": true
            })
          })
          videoStop();
          window.location.href = '/select.html';
        }
      }, 'image/png')

    } catch (err) {
      console.log('err: ', err);
      console.log('Detection failed');
    }
  }, 1000)
})

function videoStop() {
  video.pause();
  video.srcObject.getTracks()[0].stop();
  video.srcObject = null;
}

startVideo();