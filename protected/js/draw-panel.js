async function drawPanel() {
    ctx.fillStyle = "#ffefa0";
    ctx.fillRect(0, 0, 1000, 1000);

    ctx.fillStyle = "#faf3dd";
    ctx.fillRect(20, 20, 260, 490);
    ctx.fillRect(20, 870, 260, 110);

    ctx.strokeStyle = "#000000";
    ctx.lineWidth = 12;
    ctx.strokeRect(20, 20, 260, 490);
    ctx.strokeRect(20, 870, 260, 110);

    ctx.fillStyle = "#000000";
    ctx.font = "bold 40pt sans-serif";
    ctx.textAlign = "left";
    ctx.fillText("Status", 60, 95);
    ctx.fillText("Skills", 60, 190);
    ctx.fillText("Items", 60, 285);
    ctx.fillText("Shop", 60, 380);
    ctx.fillText("Setting", 60, 475);
    ctx.fillText(`$${player.gold}`, 60, 945);

    const equippedWeapon = player.items.filter((item) => { return item.type == "Weapon" && item.equipped })[0];
    const equippedClothes = player.items.filter((item) => { return item.type == "Clothes" && item.equipped })[0];
    const equippedShoes = player.items.filter((item) => { return item.type == "Shoes" && item.equipped })[0];

    ctx.strokeStyle = "#ff0000";
    ctx.lineWidth = 12;

    switch (player.panelActionIndex) {
        case 0:
            ctx.strokeRect(40, 40, 220, 70);
            break;

        case 1:
            ctx.strokeRect(40, 135, 220, 70);
            break;

        case 2:
            ctx.strokeRect(40, 230, 220, 70);
            break;

        case 3:
            ctx.strokeRect(40, 325, 220, 70);
            break;

        case 4:
            ctx.strokeRect(40, 420, 220, 70);
            break;
    }

    if (lastActionIsFinished() && panelShowingInfo == "NA") {
        if (keysDown["ArrowUp"] && player.panelActionIndex > 0) {
            updateLastActionTime();
            player.panelActionIndex -= 1;
        } else if (keysDown["ArrowDown"] && player.panelActionIndex >= 0 && player.panelActionIndex < 4) {
            updateLastActionTime();
            player.panelActionIndex += 1;
        } else if (keysDown["Escape"]) {
            updateLastActionTime();
            goMapFromPanel();
            return;
        } else if (keysDown["Space"]) {
            updateLastActionTime();
            switch (player.panelActionIndex) {
                case 0:
                    panelShowingInfo = "status";
                    break;

                case 1:
                    panelShowingInfo = "skills";
                    break;

                case 2:
                    await loadCharItems()
                    panelShowingInfo = "items";
                    break;

                case 3:
                    await getShopItems();
                    panelShowingInfo = "shop";
                    break;

                case 4:
                    panelShowingInfo = "setting";
                    break;
            }
        }
    }

    if (panelShowingInfo == "status" || panelShowingInfo == "setting") {
        ctx.fillStyle = "#faf3dd";
        ctx.fillRect(300, 20, 680, 960);

        ctx.strokeStyle = "#000000";
        ctx.lineWidth = 12;
        ctx.strokeRect(300, 20, 680, 960);
    }

    if (panelShowingInfo != "NA" && panelShowingInfo != "confirmToBuy" && panelShowingInfo != "bought" && panelShowingInfo != "failedToBuy") {
        if (keysDown["Escape"] && lastActionIsFinished()) {
            updateLastActionTime();
            panelShowingInfo = "NA";
            panelSubActionIndex = 0;
            if (panelShowingInfo == "equippedItems" || panelShowingInfo == "backpackItems") {
                await loadCharItems();
            }
        }
    }

    switch (panelShowingInfo) {
        case "status":
            ctx.drawImage(player.image, 32, 0, spriteSize, spriteSize, 544, 60, spriteSize * 6, spriteSize * 6)

            ctx.fillStyle = "#000000";
            ctx.font = "bold 30pt sans-serif";
            ctx.fillText(`Occupation: ${player.name}`, 380, 320);
            ctx.fillText(`Level: ${player.level.currentLevel}`, 380, 380);
            ctx.fillText(`Hp: ${player.hp}`, 380, 440);
            ctx.fillText(`Mp: ${player.mp}`, 380, 500)
            if (equippedWeapon) {
                ctx.fillText(`Attack: ${player.attack} + ${equippedWeapon.atk}`, 380, 560);
            } else {
                ctx.fillText(`Attack: ${player.attack}`, 380, 560);
            }

            if (equippedClothes || equippedShoes) {
                if (equippedClothes && equippedShoes) {
                    ctx.fillText(`Defence: ${player.def} + ${+equippedClothes.def + +equippedShoes.def}`, 380, 620);
                } else if (equippedClothes) {
                    ctx.fillText(`Defence: ${player.def} + ${+equippedClothes.def}`, 380, 620);
                } else {
                    ctx.fillText(`Defence: ${player.def} + ${+equippedShoes.def}`, 380, 620);
                }
            } else {
                ctx.fillText(`Defence: ${player.def}`, 380, 620);
            }

            ctx.fillText(`Exp: ${player.exp}`, 380, 680);
            ctx.fillText(`Current Monster Level: ${player.round}`, 380, 740)
            break;

        case "skills":
        case "currentSkills":
        case "skillsNotUsed":

            if (panelShowingInfo == "skills") {
                panelShowingInfo = "currentSkills"
            }

            ctx.fillStyle = "#faf3dd";
            ctx.fillRect(300, 20, 680, 250);
            ctx.fillRect(300, 290, 680, 350);
            ctx.fillRect(300, 660, 680, 320);


            ctx.strokeStyle = "#000000";
            ctx.lineWidth = 12;
            ctx.strokeRect(300, 20, 680, 250);
            ctx.strokeRect(300, 290, 680, 350);
            ctx.strokeRect(300, 660, 680, 320);

            ctx.fillStyle = "#000000";
            ctx.font = "bold 40pt sans-serif";
            ctx.textAlign = "center";
            ctx.fillText("Current Skills", 640, 95);
            ctx.fillText("Other Skills", 640, 370)

            drawListedItem(skillEquiped, 440, 120);

            let currentSkillsIds = skillEquiped.map(
                (skill) => {
                    return skill.id
                }
            )

            let otherSkills = allSkills.filter(
                (skill) => {
                    return currentSkillsIds.includes(skill.id) == false
                }
            )

            drawListedItem(otherSkills, 340, 400)

            if (panelShowingInfo == "currentSkills") {

                ctx.strokeStyle = "#ff0000";
                ctx.lineWidth = 12;
                ctx.strokeRect(440 + panelSubActionIndex * 100, 120, 100, 100);

                if (lastActionIsFinished()) {
                    if (keysDown["ArrowLeft"] && (panelSubActionIndex > 0)) {
                        updateLastActionTime();
                        panelSubActionIndex -= 1;
                    } else if (keysDown["ArrowRight"] && (panelSubActionIndex + 1 < skillEquiped.length)) {
                        updateLastActionTime();
                        panelSubActionIndex += 1;
                    } else if (keysDown["ArrowDown"]) {
                        updateLastActionTime();
                        panelShowingInfo = "skillsNotUsed"
                        panelSubActionIndex = 0;
                    } else if (keysDown["Space"] && skillEquiped[panelSubActionIndex]) {
                        updateLastActionTime();
                        skillEquiped.splice(panelSubActionIndex, 1);
                    }
                }

                showSkillInfo(skillEquiped);
            }

            if (panelShowingInfo == "skillsNotUsed") {
                ctx.strokeStyle = "#ff0000";
                ctx.lineWidth = 12;
                showRedBoxForItemSelection(340, 400);

                if (lastActionIsFinished()) {
                    if (keysDown["ArrowUp"] && panelSubActionIndex <= 5) {
                        updateLastActionTime();
                        panelShowingInfo = "currentSkills";
                        panelSubActionIndex = 0;
                    }
                    controlArrowKeysDownForItemSelection(otherSkills);

                    if (keysDown["Space"] && skillEquiped.length < 4 && otherSkills[panelSubActionIndex]) {
                        updateLastActionTime();

                        skillEquiped.push(otherSkills[panelSubActionIndex]);
                    }
                }

                showSkillInfo(otherSkills);
            }

            break;

        case "items":
        case "equippedItems":
        case "backpackItems":

            if (panelShowingInfo == "items") {
                panelShowingInfo = "equippedItems";
            }

            ctx.fillStyle = "#faf3dd";
            ctx.fillRect(300, 20, 680, 300);
            ctx.fillRect(300, 340, 680, 350);
            ctx.fillRect(300, 710, 680, 270);

            ctx.strokeStyle = "#000000";
            ctx.lineWidth = 12;
            ctx.strokeRect(300, 20, 680, 300);
            ctx.strokeRect(300, 340, 680, 350);
            ctx.strokeRect(300, 710, 680, 270);

            ctx.fillStyle = "#000000";
            ctx.font = "bold 40pt sans-serif";
            ctx.textAlign = "center";

            ctx.fillText(`Equipped Items`, 640, 95);
            ctx.fillText(`Backpack Items`, 640, 420);

            let equippedItems = [0, 0, 0];

            if (equippedWeapon) {
                const [sourceX, sourceY] = JSON.parse(equippedWeapon.image_index);
                ctx.drawImage(itemImage, sourceX * spriteSize, sourceY * spriteSize, spriteSize, spriteSize, 390, 120, 100, 100)
                equippedItems[0] = equippedWeapon;
            }

            if (equippedClothes) {
                const [sourceX, sourceY] = JSON.parse(equippedClothes.image_index);
                ctx.drawImage(itemImage, sourceX * spriteSize, sourceY * spriteSize, spriteSize, spriteSize, 590, 120, 100, 100)
                equippedItems[1] = equippedClothes;
            }

            if (equippedShoes) {
                const [sourceX, sourceY] = JSON.parse(equippedShoes.image_index);
                ctx.drawImage(itemImage, sourceX * spriteSize, sourceY * spriteSize, spriteSize, spriteSize, 790, 120, 100, 100)
                equippedItems[2] = equippedShoes;
            }

            ctx.font = "bold 32pt sans-serif";
            ctx.fillText(`Weapon`, 440, 280);
            ctx.fillText(`Clothes`, 640, 280);
            ctx.fillText(`Shoes`, 840, 280);

            let backpackItems = player.items.filter((item) => { return !item.equipped && item.quantity > 0 });

            drawListedItem(backpackItems, 340, 450);

            if (panelShowingInfo == "equippedItems") {
                ctx.strokeStyle = "#ff0000";
                ctx.lineWidth = 12;
                switch (panelSubActionIndex) {
                    case 0:
                        ctx.strokeRect(390, 120, 100, 100);
                        break;

                    case 1:
                        ctx.strokeRect(590, 120, 100, 100);
                        break;

                    case 2:
                        ctx.strokeRect(790, 120, 100, 100);
                        break;
                }

                if (lastActionIsFinished()) {
                    if (keysDown["ArrowLeft"] && (panelSubActionIndex > 0)) {
                        updateLastActionTime();
                        panelSubActionIndex -= 1;
                    } else if (keysDown["ArrowRight"] && (panelSubActionIndex < 2)) {
                        updateLastActionTime();
                        panelSubActionIndex += 1;
                    } else if (keysDown["ArrowDown"]) {
                        updateLastActionTime();
                        panelShowingInfo = "backpackItems"
                        panelSubActionIndex = 0;
                    } else if (keysDown["Space"] && equippedItems[panelSubActionIndex]) {
                        updateLastActionTime();
                        equippedItems[panelSubActionIndex].equipped = false;
                        await updateEquippedStatus(equippedItems[panelSubActionIndex].id, equippedItems[panelSubActionIndex].equipped)
                    }
                }

                showItemInfo(equippedItems);

            }

            if (panelShowingInfo == "backpackItems") {
                showRedBoxForItemSelection(340, 450);

                if (lastActionIsFinished()) {
                    if (keysDown["ArrowUp"] && panelSubActionIndex <= 5) {
                        updateLastActionTime();
                        panelShowingInfo = "equippedItems";
                        panelSubActionIndex = 0;
                    }
                    controlArrowKeysDownForItemSelection(backpackItems);
                    if (keysDown["Space"] && backpackItems[panelSubActionIndex]) {
                        updateLastActionTime();
                        switch (backpackItems[panelSubActionIndex].type) {
                            case "Weapon":
                                if (!equippedWeapon) {
                                    backpackItems[panelSubActionIndex].equipped = true;
                                    await updateEquippedStatus(backpackItems[panelSubActionIndex].id, backpackItems[panelSubActionIndex].equipped)
                                }
                                break;

                            case "Clothes":
                                if (!equippedClothes) {
                                    backpackItems[panelSubActionIndex].equipped = true;
                                    await updateEquippedStatus(backpackItems[panelSubActionIndex].id, backpackItems[panelSubActionIndex].equipped)
                                }
                                break;

                            case "Shoes":
                                if (!equippedShoes) {
                                    backpackItems[panelSubActionIndex].equipped = true;
                                    await updateEquippedStatus(backpackItems[panelSubActionIndex].id, backpackItems[panelSubActionIndex].equipped)
                                }
                                break;

                            case "Potion":
                                backpackItems[panelSubActionIndex].quantity -= 1;
                                await useItem(backpackItems[panelSubActionIndex].id);
                                await updateHpMp(player.hp, player.mp)
                                break;
                        }
                    }
                }

                showItemInfo(backpackItems);
            }

            break;

        case "shop":
        case "confirmToBuy":
        case "bought":
        case "failedToBuy":
            ctx.fillStyle = "#faf3dd";
            ctx.fillRect(300, 20, 680, 380);
            ctx.fillRect(300, 420, 680, 430);

            ctx.strokeStyle = "#000000";
            ctx.lineWidth = 12;
            ctx.strokeRect(300, 20, 680, 380);
            ctx.strokeRect(300, 420, 680, 430);

            drawListedItem(itemList, 340, 60);

            showRedBoxForItemSelection(340, 60);

            if (lastActionIsFinished() && panelShowingInfo == "shop") {

                controlArrowKeysDownForItemSelection(itemList);

                if (keysDown["Space"] && itemList[panelSubActionIndex]) {
                    updateLastActionTime();
                    panelShowingInfo = "confirmToBuy"
                }
            }

            ctx.fillStyle = "#000000";
            ctx.font = "bold 40pt sans-serif";
            ctx.textAlign = "center";
            ctx.fillText(`${itemList[panelSubActionIndex].name}`, 640, 510);

            //Need to revise below to show item type and also show correct info for consumables
            ctx.textAlign = "left";
            ctx.fillText(`Type: ${itemList[panelSubActionIndex].type}`, 340, 590);
            if (itemList[panelSubActionIndex].id == 8) {
                ctx.fillText(`Heal: ${itemList[panelSubActionIndex].atk}MP`, 340, 650);
            } else if (itemList[panelSubActionIndex].id == 9) {
                ctx.fillText(`Heal: ${itemList[panelSubActionIndex].atk}HP`, 340, 650);
            } else if (itemList[panelSubActionIndex].atk > 0) {
                ctx.fillText(`Attack: ${itemList[panelSubActionIndex].atk}`, 340, 650);
            } else {
                ctx.fillText(`Defence: ${itemList[panelSubActionIndex].def}`, 340, 650);
            }

            ctx.fillText(`Price: ${itemList[panelSubActionIndex].gold}`, 340, 710);

            if (panelShowingInfo == "confirmToBuy") {
                ctx.fillStyle = "#faf3dd";
                ctx.fillRect(300, 870, 680, 110);

                ctx.strokeStyle = "#000000";
                ctx.lineWidth = 12;
                ctx.strokeRect(300, 870, 680, 110);

                ctx.fillStyle = "#000000";
                ctx.font = "bold 40pt sans-serif";
                ctx.fillText("Confirm to buy this item", 330, 945);

                ctx.strokeStyle = "#ff0000";
                ctx.lineWidth = 12;
                ctx.strokeRect(320, 890, 640, 70);

                if (keysDown["Escape"]) {
                    updateLastActionTime();
                    panelShowingInfo = "shop";
                } else if (keysDown["Space"] && lastActionIsFinished()) {
                    updateLastActionTime();
                    let json = await buyItems(itemList[panelSubActionIndex].id)
                    await checkCharGold();
                    if (json.outcome == "ok") {
                        await loadCharItems();
                        panelShowingInfo = "bought";
                    } else if (json.outcome = "failed") {
                        panelShowingInfo = "failedToBuy"
                    }
                    await loadCharItems();
                    return
                }
            }

            if (panelShowingInfo == "bought") {

                if (phraseFrameCount < 90) {
                    ctx.fillStyle = "#faf3dd";
                    ctx.fillRect(125, 380, 750, 180);

                    ctx.strokeStyle = "#000000";
                    ctx.lineWidth = 12;
                    ctx.strokeRect(125, 380, 750, 180);
                    ctx.fillStyle = "#000000";
                    ctx.font = "bold 40pt sans-serif";
                    wrapText("You've purchased the item successfully.", 155, 485, 690, 40);
                    phraseFrameCount++;
                } else {
                    phraseFrameCount = 0;
                    panelShowingInfo = "shop";
                }
            }

            if (panelShowingInfo == "failedToBuy") {
                if (phraseFrameCount < 90) {
                    ctx.fillStyle = "#faf3dd";
                    ctx.fillRect(125, 380, 750, 180);

                    ctx.strokeStyle = "#000000";
                    ctx.lineWidth = 12;
                    ctx.strokeRect(125, 380, 750, 180);
                    ctx.fillStyle = "#000000";
                    ctx.font = "bold 40pt sans-serif";
                    wrapText("You don't have enough gold to buy the item.", 155, 485, 690, 40);
                    phraseFrameCount++;
                } else {
                    phraseFrameCount = 0;
                    panelShowingInfo = "shop";
                }
            }

            break;

        case "setting":
            ctx.fillStyle = "#000000";
            ctx.font = "bold 40pt sans-serif";
            ctx.fillText("Music: ", 340, 95);
            ctx.fillText("ON", 560, 95);
            ctx.fillText("OFF", 700, 95);

            ctx.strokeStyle = "#ff0000";
            ctx.lineWidth = 12;

            if (musicMuted == 0) {
                ctx.strokeRect(540, 40, 120, 72);
                battleSoundtrack.muted = false;
                mapSoundtrack.muted = false;
                panelSoundtrack.muted = false;
                gameOverSoundTrack.muted = false;
                if (keysDown["ArrowRight"]) {
                    musicMuted = 1;
                }
            } else {
                ctx.strokeRect(685, 40, 140, 72);
                battleSoundtrack.muted = true;
                mapSoundtrack.muted = true;
                panelSoundtrack.muted = true;
                gameOverSoundTrack.muted = true;
                if (keysDown["ArrowLeft"]) {
                    musicMuted = 0;
                }
            }
            break;
    }

}

async function goMapFromPanel() {
    player.placeAt(player.tileTo[0], player.tileTo[1]);
    gameMode = "map";
    panelShowingInfo = "NA"
    panelSubActionIndex = 0;
    resizeCanvasForMap();
    panelSoundtrack.load();
    mapSoundtrack.play();
}

function drawListedItem(items, positionX, positionY) {
    let itemColumn = 0;
    let itemRow = 0;
    for (let item of items) {
        let imageIndex = JSON.parse(item.image_index);
        ctx.drawImage(itemImage, imageIndex[0] * 32, imageIndex[1] * 32, spriteSize, spriteSize, positionX + itemColumn * 100, positionY + itemRow * 100, 100, 100)
        itemColumn++;
        if (itemColumn == 6) {
            itemColumn = 0;
            itemRow++;
        }
    }
}

function showRedBoxForItemSelection(positionX, positionY) {
    ctx.strokeStyle = "#ff0000";
    ctx.lineWidth = 12;

    let indexColumn = panelSubActionIndex % 6;
    let indexRow = (panelSubActionIndex - indexColumn) / 6;

    ctx.strokeRect(positionX + 100 * indexColumn, positionY + 100 * indexRow, 100, 100);
}

function controlArrowKeysDownForItemSelection(items) {
    if (keysDown["ArrowUp"] && panelSubActionIndex > 5) {
        updateLastActionTime();
        panelSubActionIndex -= 6;
    } else if (keysDown["ArrowDown"] && (panelSubActionIndex + 6 < items.length)) {
        updateLastActionTime();
        panelSubActionIndex += 6;
    } else if (keysDown["ArrowLeft"] && (panelSubActionIndex % 6 != 0)) {
        updateLastActionTime();
        panelSubActionIndex -= 1;
    } else if (keysDown["ArrowRight"] && (panelSubActionIndex % 6 != 5) && (panelSubActionIndex + 1 < items.length)) {
        updateLastActionTime();
        panelSubActionIndex += 1;
    }
}

function showItemInfo(items) {

    if (items[panelSubActionIndex]) {
        ctx.fillStyle = "#000000";
        ctx.font = "bold 40pt sans-serif";
        ctx.textAlign = "center";
        ctx.fillText(`${items[panelSubActionIndex].name}`, 640, 790);

        ctx.textAlign = "left";
        ctx.fillText(`Type: ${items[panelSubActionIndex].type}`, 340, 860);

        if (items[panelSubActionIndex].item_id == 8) {
            ctx.fillText(`Heal: ${items[panelSubActionIndex].atk}MP`, 340, 930);
        } else if (items[panelSubActionIndex].item_id == 9) {
            ctx.fillText(`Heal: ${items[panelSubActionIndex].atk}HP`, 340, 930);
        } else if (items[panelSubActionIndex].atk > 0) {
            ctx.fillText(`Attack: ${items[panelSubActionIndex].atk}`, 340, 930);
        } else {
            ctx.fillText(`Defence: ${items[panelSubActionIndex].def}`, 340, 930);
        }

        ctx.fillText(`Quantity: ${items[panelSubActionIndex].quantity}`, 650, 930);

    }

}

function showSkillInfo(skills) {
    if (skills[panelSubActionIndex]) {
        ctx.fillStyle = "#000000";
        ctx.font = "bold 40pt sans-serif";
        ctx.textAlign = "center";
        ctx.fillText(`${skills[panelSubActionIndex].name}`, 640, 740);

        ctx.font = "bold 32pt sans-serif";
        ctx.textAlign = "left";
        ctx.fillText(`MP: ${skills[panelSubActionIndex].mp}`, 340, 810);

        if (skills[panelSubActionIndex].heal > 0) {
            ctx.fillText(`Heal: ${skills[panelSubActionIndex].heal} HP`, 340, 880);
        } else if (skills[panelSubActionIndex].dmg > 0 || skills[panelSubActionIndex].dmgR > 0) {
            wrapText(`Damage: (${skills[panelSubActionIndex].dmg} + ${player.attack} ) x ${skills[panelSubActionIndex].dmgR}`, 340, 880, 600, 20);
        }
    }
}