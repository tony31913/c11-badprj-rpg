function drawGameOver() {
    ctx.fillStyle = "#000000";
    ctx.fillRect(0, 0, 1000, 1000);
    ctx.textAlign = "center";

    ctx.drawImage(gameOverImage, 0, 0, 512, 512, 295, 150, 410, 410)

    ctx.fillStyle = "#ff0000";

    ctx.font = "bold 100pt sans-serif";
    ctx.fillText(`GAME OVER`, 500, 710);

    ctx.fillStyle = "#ffffff";

    ctx.font = "bold 50pt sans-serif";
    ctx.fillText(`Press Space to Restart`, 500, 810);

    if (keysDown["Space"] && lastActionIsFinished()) {
        updateLastActionTime();
        window.location.assign("/select.html")
        gameOverSoundTrack.load();
    }
}