class Animation {
    constructor() {
        this.frameCount = 0;
        this.noOfFramesToChangeImage = 10;
        this.frameIndex = 0;
        this.frameSetIndex = 0;
    }

    change = (frameSetIndex) => {
        if (this.frameSetIndex != frameSetIndex) {
            this.frameCount = 0;
            this.frameIndex = 0;
            this.frameSetIndex = frameSetIndex;
        }
    }

    update = () => {
        this.frameCount++;

        if (this.frameCount >= this.noOfFramesToChangeImage) {
            this.frameCount = 0;
            this.frameIndex = (this.frameIndex == 2) ? 0 : this.frameIndex + 1;
        }
    }

    remainStatic = () => {
        this.frameCount = 0;
        this.frameIndex = 0;
        this.frameSetIndex = 0;
    }
}

class Character {
    constructor(tileFrom, tileTo, imageSrc) {
        this.tileFrom = tileFrom; // To record the origin in map index
        this.tileTo = tileTo; // To record the destination in map index
        this.lastActionTime = Date.now(); // The time last movement is triggered; after a movement is complete, it will be updated as new time.
        this.position = [((tileLength * this.tileFrom[0]) + ((tileLength - spriteSize) / 2)),
        ((tileLength * this.tileFrom[1]) + ((tileLength - spriteSize) / 2))]; // The actual position of the character in pixels
        this.timeNeededForAnAction = 500; // Milliseconds needed to process a single movement 
        this.battleActionIndex = 0;
        this.panelActionIndex = 0;
        this.animation = new Animation();
        this.image = new Image();
        this.image.src = imageSrc;
        this.level = new Level();
        this.items = charItems;
        // [
        //     { id: 1, name: "Long Sword", def: 0, atk: 10, gold: 10, image_index: "[1, 5]", type: "Weapon", equipped: true, quantity: 1 },
        //     { id: 2, name: "Clothes", def: 4, atk: 0, gold: 5, image_index: "[7, 7]", type: "Clothes", equipped: true, quantity: 1 },
        //     { id: 3, name: "Health Potion", def: 0, atk: 10, gold: 5, image_index: "[0, 9]", type: "Consumable", quantity: 2 },
        //     { id: 4, name: "Armor", def: 10, atk: 0, gold: 10, image_index: "[4, 7]", type: "Clothes", equipped: false, quantity: 1 },
        //     { id: 5, name: "Axe", def: 0, atk: 12, gold: 14, image_index: "[1, 10]", type: "Weapon", equipped: false, quantity: 1 },
        //     { id: 6, name: "MP Potion", def: 0, atk: 10, gold: 5, image_index: "[2, 9]", type: "Consumable", quantity: 3 },
        //     { id: 7, name: "Leather Boots", def: 4, atk: 0, gold: 5, image_index: "[2, 8]", type: "Shoes", equipped: true, quantity: 1 },
        //     { id: 8, name: "Steel Boots", def: 6, atk: 0, gold: 10, image_index: "[3, 8]", type: "Shoes", equipped: false, quantity: 1 }
        // ]
    }

    // Below method is to update the tileFrom and position after a movement is completed.
    placeAt = (x, y) => {
        this.tileFrom = [x, y];
        this.tileTo = [x, y];
        this.position = [((tileLength * x) + ((tileLength - spriteSize) / 2)),
        ((tileLength * y) + ((tileLength - spriteSize) / 2))];
    }

    processMovement = () => {
        if (this.tileFrom[0] == this.tileTo[0] && this.tileFrom[1] == this.tileTo[1]) { //It means the character is not moving
            return false;
        }

        // It is to check whether the last movement has been completed. If yes, update the tileFrom and the position as destination location.
        if (lastActionIsFinished()) {
            this.placeAt(this.tileTo[0], this.tileTo[1]);
        }
        // If the movement hasn't completed, we need to calculate the actual position of the character of the current frame
        else {
            if (this.tileTo[0] != this.tileFrom[0]) {
                let diff = tileLength / (this.timeNeededForAnAction / 16.7); // To calculate the diff between each frame
                this.position[0] += (this.tileTo[0] < this.tileFrom[0] ? 0 - diff : diff);
            }
            if (this.tileTo[1] != this.tileFrom[1]) {
                let diff = tileLength / (this.timeNeededForAnAction / 16.7);
                this.position[1] += (this.tileTo[1] < this.tileFrom[1] ? 0 - diff : diff);
            }
        }

        // It means the movement is still being processed. 
        return true;
    }
    updateData = (result) => {

        this.hp = result[0].hp;
        this.mp = result[0].mp;
        this.attack = result[0].atk;
        this.def = result[0].def;
        this.gold = result[0].gold;
        this.id = result[0].id;
        this.exp = result[0].exp;
        this.level.currentLevel = result[0].level;
        this.name = result[0].name;
        this.round = result[0].round
    }

    updateGold = (result) => {
        this.gold = result
    }

    updateHP = (result) => {
        this.hp += result[0].atk
    }
    updateMP = (result) => {
        this.mp += result[0].atk
    }
};


class Enemy {
    constructor(id, tileFrom, tileTo, imageSrc, alive) {
        this.tileFrom = tileFrom; // To record the origin in map index
        this.tileTo = tileTo;
        this.position = [((tileLength * this.tileFrom[0]) + ((tileLength - spriteSize) / 2)),
        ((tileLength * this.tileFrom[1]) + ((tileLength - spriteSize) / 2))];
        this.image = new Image();
        this.image.src = imageSrc;
        this.animation = new Animation();
        this.hp = 0;
        this.attack = 0;
        this.id = id;
        this.alive = alive
    }

    updateData = (result) => {

        this.hp = result[0].hp;
        this.attack = result[0].atk;
        this.id = result[0].id
    }


}

class item {
    constructor(name, quantity, atk, def) {
        this.name = name
        this.quantity = quantity
        this.atk = atk
        this.def = def
    }
}

class Level {
    constructor() {
        this.currentLevel;
        this.pastLevel;
        this.newLevel;
    }
}