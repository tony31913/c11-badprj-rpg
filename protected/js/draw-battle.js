async function drawBattle() {

    ctx.fillStyle = "#ffefa0";
    ctx.fillRect(0, 0, 1000, 1000);
    ctx.textAlign = "center";

    ctx.drawImage(player.image, 0, 0, spriteSize, spriteSize, 254, 100, spriteSize * 6, spriteSize * 6)

    ctx.drawImage(currentMonster.img, 32, 0, spriteSize, spriteSize, 554, 100, spriteSize * 6, spriteSize * 6)

    ctx.fillStyle = "#faf3dd";
    ctx.fillRect(20, 460, 960, 520);

    ctx.strokeStyle = "#000000";
    ctx.lineWidth = 12;
    ctx.strokeRect(20, 460, 960, 520);


    if (battlePhrase == "selectItem") {

        let itemOptions = player.items.filter((item) => { return item.type == "Potion" && item.quantity > 0 });

        displayBattleActions(itemOptions);
        if (keysDown["ArrowUp"] && player.battleActionIndex > 1) {
            player.battleActionIndex -= 2;
        } else if (keysDown["ArrowDown"] && player.battleActionIndex < 2 && (player.battleActionIndex + 2 < itemOptions.length)) {
            player.battleActionIndex += 2;
        } else if (keysDown["ArrowLeft"] && player.battleActionIndex % 2 == 1) {
            player.battleActionIndex -= 1;
        } else if (keysDown["ArrowRight"] && player.battleActionIndex % 2 == 0 && (player.battleActionIndex + 1 < itemOptions.length)) {
            player.battleActionIndex += 1;
        } else if (keysDown["Space"] && lastActionIsFinished() && itemOptions[player.battleActionIndex]) {
            updateLastActionTime();

            await battleUseItem(itemOptions[player.battleActionIndex].item_id, currentMonster.id)
            await loadCharItems()
            //We can use itemOptions[player.battleActionIndex].id to fetch here.
            battlePhrase = "playerHeals"



            player.battleActionIndex = 0;
        } else if (keysDown["Escape"]) {
            battlePhrase = "playerSelectAction";
            player.battleActionIndex = 0;
        }
    } else if (battlePhrase == "selectSkill") {

        let adjustedSkillOptions = skillEquiped.filter((skill) => { return player.mp >= skill.mp })

        displayBattleActions(adjustedSkillOptions)
        if (keysDown["ArrowUp"] && player.battleActionIndex > 1) {
            player.battleActionIndex -= 2;
        } else if (keysDown["ArrowDown"] && player.battleActionIndex < 2 && (player.battleActionIndex + 2 < adjustedSkillOptions.length)) {
            player.battleActionIndex += 2;
        } else if (keysDown["ArrowLeft"] && player.battleActionIndex % 2 == 1) {
            player.battleActionIndex -= 1;
        } else if (keysDown["ArrowRight"] && player.battleActionIndex % 2 == 0 && (player.battleActionIndex + 1 < adjustedSkillOptions.length)) {
            player.battleActionIndex += 1;
        } else if (keysDown["Space"] && lastActionIsFinished() && adjustedSkillOptions[player.battleActionIndex]) {
            updateLastActionTime();

            usedSkill = adjustedSkillOptions[player.battleActionIndex];


            await useSkill(currentMonster.id, usedSkill.id)
            if (usedSkill.is_healing) {
                healthChange = +usedSkill.heal;
                battlePhrase = "playerHeals"
            } else {
                healthChange = Math.round((+usedSkill.dmg + (+player.attack)) * +usedSkill.dmgR - (+currentMonster.def));
                battlePhrase = "playerAttacks"
            }

            usedSkill = usedSkill.name;

            player.battleActionIndex = 0;
            return
        } else if (keysDown["Escape"]) {
            battlePhrase = "playerSelectAction";
            player.battleActionIndex = 0;
        }
    } else if (battlePhrase == "playerSelectAction") {
        displayBattleActions(battleActions);


        if (keysDown["ArrowUp"] && player.battleActionIndex > 1) {
            player.battleActionIndex -= 2;
        } else if (keysDown["ArrowDown"] && player.battleActionIndex < 2 && (player.battleActionIndex + 2 < battleActions.length)) {
            player.battleActionIndex += 2;
        } else if (keysDown["ArrowLeft"] && player.battleActionIndex % 2 == 1) {
            player.battleActionIndex -= 1;
        } else if (keysDown["ArrowRight"] && player.battleActionIndex % 2 == 0 && (player.battleActionIndex + 1 < battleActions.length)) {
            player.battleActionIndex += 1;
        } else if (keysDown["Space"] && lastActionIsFinished()) {
            updateLastActionTime();
            switch (player.battleActionIndex) {
                case 0:
                    usedSkill = "Attack";
                    healthChange = +player.attack - (+currentMonster.def);
                    battlePhrase = "playerAttacks";
                    //done
                    await autoAttack(currentMonster.id)
                    break;

                case 1:
                    battlePhrase = "selectSkill";
                    player.battleActionIndex = 0;
                    break;

                case 2:
                    //unfinish
                    //We can use fetch to update itemOptions here.
                    battlePhrase = "selectItem";
                    player.battleActionIndex = 0;
                    break;

                case 3:

                    await escapeFunction(currentMonster.id);
                    await loadPlayer();
                    battlePhrase = "escape";
                    await moveToTheNearestLocation();
                    goMapFromBattle();
                    // await escapeFunction(currentMonster.id)
                    return;
            }
        }
    }

    ctx.fillStyle = "#ff0000";

    ctx.font = "bold 40pt sans-serif";
    ctx.fillText(`HP: ${player.hp < 0 ? 0 : player.hp}`, 350, 360);
    ctx.fillText(`HP: ${currentMonster.hp < 0 ? 0 : currentMonster.hp}`, 650, 360);

    ctx.fillStyle = "#0000ff";
    ctx.fillText(`MP: ${player.mp < 0 ? 0 : player.mp}`, 350, 430);

    if (battlePhrase == "selectItem" || battlePhrase == "selectSkill" || battlePhrase == "playerSelectAction") {
        ctx.strokeStyle = "#ff0000";
        ctx.lineWidth = 12;
        switch (player.battleActionIndex) {
            case 0:
                ctx.strokeRect(60, 500, 420, 200);
                break;

            case 1:
                ctx.strokeRect(520, 500, 420, 200);
                break;

            case 2:
                ctx.strokeRect(60, 740, 420, 200);
                break;

            case 3:
                ctx.strokeRect(520, 740, 420, 200);
                break;
        }
    }


    if (battlePhrase == "playerHeals") {
        if (phraseFrameCount < 60) {
            ctx.fillStyle = "#228B22";
            ctx.beginPath();
            ctx.moveTo(92, 260);
            ctx.lineTo(127, 210);
            ctx.lineTo(162, 260);
            ctx.fill();
            ctx.font = "bold 40pt sans-serif";
            ctx.fillText(`HP ${healthChange}`, 127, 320);
            ctx.fillStyle = "#000000";
            ctx.font = "bold 50pt sans-serif";
            ctx.fillText(`Player Turn:`, 500, 700);
            ctx.fillText(`${usedSkill}`, 500, 800);
            phraseFrameCount++;
        } else {
            phraseFrameCount = 0;
            battlePhrase = "enemyAttacks";
            //counterAttack here
            await counterAttack(currentMonster.id)
        }
    } else if (battlePhrase == "playerAttacks") {
        if (phraseFrameCount < 60) {
            ctx.strokeStyle = "#ff0000";
            ctx.lineWidth = 25;
            ctx.beginPath();
            ctx.moveTo(780, 100);
            ctx.lineTo(520, 320);
            ctx.stroke();
            ctx.fillStyle = "#ff0000";
            ctx.beginPath();
            ctx.moveTo(838, 210);
            ctx.lineTo(873, 260);
            ctx.lineTo(908, 210);
            ctx.fill();
            ctx.font = "bold 40pt sans-serif";
            ctx.fillText(`HP ${healthChange}`, 873, 320);

            ctx.fillStyle = "#000000";
            ctx.font = "bold 50pt sans-serif";
            ctx.fillText(`Player Turn:`, 500, 700);
            ctx.fillText(`${usedSkill}`, 500, 800);
            phraseFrameCount++;
        } else {
            phraseFrameCount = 0;

            if (currentMonster.hp > 0) {
                battlePhrase = "enemyAttacks";

                //counterAttack here
                await counterAttack(currentMonster.id)

            } else {
                battlePhrase = "win";
            }
        }
    } else if (battlePhrase == "enemyAttacks") {
        if (phraseFrameCount < 60) {
            ctx.strokeStyle = "#ff0000";
            ctx.lineWidth = 25;
            ctx.beginPath();
            ctx.moveTo(480, 100);
            ctx.lineTo(220, 320);
            ctx.stroke();

            ctx.fillStyle = "#ff0000";
            ctx.beginPath();
            ctx.moveTo(92, 210);
            ctx.lineTo(127, 260);
            ctx.lineTo(162, 210);
            ctx.fill();
            ctx.font = "bold 40pt sans-serif";
            ctx.fillText(`HP ${healthChange}`, 127, 320);
            ctx.fillStyle = "#000000";
            ctx.font = "bold 50pt sans-serif";
            ctx.fillText(`Enemy Turn:`, 500, 700);
            ctx.fillText(`${usedSkill}`, 500, 800);

            phraseFrameCount++;
        } else {
            phraseFrameCount = 0;

            if (player.hp > 0) {
                battlePhrase = "playerSelectAction";
            } else {
                battlePhrase = "lose";
            }

        }
    } else if (battlePhrase == "win") {
        if (phraseFrameCount < 120) {
            ctx.fillStyle = "#1f3c88";
            ctx.font = "bold 160pt sans-serif";
            ctx.fillText(`Victory!`, 500, 320);
            ctx.textAlign = "left";
            ctx.fillStyle = "#000000";
            ctx.font = "bold 50pt sans-serif";
            wrapText(`You've gained $${currentMonster.gold} and ${currentMonster.expPoint} Exp.`, 80, 640, 840, 50);

            if (player.level.newLevel > player.level.pastLevel) {
                wrapText(`Congratulations! You've upgraded to level ${player.level.newLevel}.`, 80, 840, 840, 50);
            }
            phraseFrameCount++;
        } else {
            await loadAllMonster();
            await loadPlayer();
            goMapFromBattle();
        }
    } else if (battlePhrase == "lose") {
        if (phraseFrameCount < 120) {
            ctx.fillStyle = "#bb2205";
            ctx.font = "bold 160pt sans-serif";
            ctx.fillText(`Defeat!`, 500, 320);
            ctx.textAlign = "left";
            ctx.fillStyle = "#000000";
            ctx.font = "bold 50pt sans-serif";
            wrapText(`You've lost everything!`, 80, 590, 840, 50);
            phraseFrameCount++
        } else {
            battleSoundtrack.load();
            gameMode = "gameover"
            gameOverSoundTrack.play();
        }
    }
}

async function goMapFromBattle() {
    gameMode = "map";
    phraseFrameCount = 0;
    resizeCanvasForMap();
    battleSoundtrack.load();
    mapSoundtrack.play();
}

function displayBattleActions(options) {

    battleOptionArray = [];

    const coordinate = [[270, 625], [730, 625], [270, 865], [730, 865]];

    ctx.fillStyle = "#000000";
    ctx.font = "bold 50pt sans-serif";

    for (let i in options) {
        wrapText(options[i].name, coordinate[i][0], coordinate[i][1], 420, 50)
    }
}

function wrapText(text, x, y, maxWidth, lineHeight) {
    var words = text.split(' ');
    var line = '';

    for (var n = 0; n < words.length; n++) {
        var testLine = line + words[n] + ' ';
        var metrics = ctx.measureText(testLine);
        var testWidth = metrics.width;
        if (testWidth > maxWidth && n > 0) {
            ctx.fillText(line, x, y - lineHeight);
            line = words[n] + ' ';
            y += lineHeight;
        }
        else {
            line = testLine;
        }
    }
    ctx.fillText(line, x, y);
}

//testing attack function



async function autoAttack(monsterId) {
    const res = await fetch('/autoAttack', {
        method: 'put',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "monsterId": monsterId
        })
    })

    const result = await res.json()


    player.level.pastLevel = result[0].pastLevel;
    player.level.newLevel = result[0].newLevel;

    player.updateData([result[0]])

    currentMonster.hp = result[1].hp


    allMonster = []
    await loadAllMonster()
}


async function counterAttack(monsterId) {
    const res = await fetch('/counterAttack', {
        method: 'put',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "monsterId": monsterId
        })
    })

    const result = await res.json()
    usedSkill = result[2].usingSkill
    healthChange = result[2].damageDone

    if (result[0].alive) {
        player.updateData([result[0]])
        currentMonster.hp = result[1].hp
    } else {
        player.updateData([result[0]])
        battlePhrase = "lose"
    }

}

async function moveToTheNearestLocation() {
    let x = player.tileFrom[0];
    let y = player.tileFrom[1];
    if (gameMap[checkIndexInMap(x - 1, y)] == 1) {
        player.placeAt(x - 1, y);
    } else if (gameMap[checkIndexInMap(x, y - 1)] == 1) {
        player.placeAt(x, y - 1);
    } else if (gameMap[checkIndexInMap(x + 1, y)] == 1) {
        player.placeAt(x + 1, y);
    } else {
        player.placeAt(x, y + 1);
    }

    await saveData(player.tileFrom)
}

async function saveData(location) {

    let currentSkillsIds = skillEquiped.map(
        (skill) => {
            return skill.id
        }
    )

    await fetch('/savaData', {
        method: 'put',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "location": location,
            "skills": currentSkillsIds,
            //    "items" : itemEquip
        })
    })
}

async function escapeFunction(monsterId) {
    await fetch('/escapeFunction', {
        method: 'put',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "monsterId": monsterId,
            'playerHp': player.hp,
            'playerMp': player.mp
        })
    })
}


async function useSkill(monsterId, skillId) {
    const res = await fetch('/useSkill', {
        method: 'put',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            'monsterId': monsterId,
            'skillId': skillId
        })
    })

    const result = await res.json()



    player.level.pastLevel = result[0].pastLevel;
    player.level.newLevel = result[0].newLevel;


    player.updateData([result[0]])

    currentMonster.hp = result[1].hp


    allMonster = []
    await loadAllMonster()

}


async function battleUseItem(itemId, monsterId) {
    const res = await fetch('/battleUseItem', {
        method: 'put',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            'monsterId': monsterId,
            'itemId': itemId
        })
    })

    const result = await res.json()

    player.updateData([result[0]])

    currentMonster.hp = result[1].hp
}