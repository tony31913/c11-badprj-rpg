// To check the index of the tile in gameMap array
function checkIndexInMap(x, y) {
    return ((y * columns) + x);
}

async function drawMap() {


    //when hit the location of the monster
    for (let monster of allMonster) {
        if (monster.alive) {
            if (player.tileFrom[0] == monster.tileFrom[0] && player.tileFrom[1] == monster.tileFrom[1]) {

                await loadMonster(monster.id);
                await loadPlayer();
                await loadCharItems();
                const equippedWeapon = player.items.filter((item) => { return item.type == "Weapon" && item.equipped })[0];
                if (equippedWeapon) {
                    player.attack = +player.attack + (+equippedWeapon.atk)
                }

                gameMode = "battle";
                resizeCanvasForBattleAndPanel();
                player.battleActionIndex = 0;
                battlePhrase = "playerSelectAction";
                mapSoundtrack.pause();
                battleSoundtrack.play();
                return;
            }
        }
    }

    ctx.font = "bold 16pt sans-serif";

    if (keysDown["Escape"] && lastActionIsFinished()) {
        updateLastActionTime();
        await saveData(player.tileFrom)
        await loadPlayer();
        await loadCharItems();
        mapSoundtrack.pause();
        panelSoundtrack.play();
        gameMode = "panel";
        player.panelActionIndex = 0;
        resizeCanvasForBattleAndPanel();
        return;
    }

    if (player.tileFrom[0] == player.tileTo[0] && player.tileFrom[1] == player.tileTo[1]) {
        if (keysDown["ArrowUp"]) {
            player.animation.change(3);
        } else if (keysDown["ArrowDown"]) {
            player.animation.change(0);
        } else if (keysDown["ArrowLeft"]) {
            player.animation.change(1);
        } else if (keysDown["ArrowRight"]) {
            player.animation.change(2);
        }
    }

    if (!player.processMovement()) { // If no movement is being processed, we accept new instructions from the player.
        if (keysDown["ArrowUp"] && player.tileFrom[1] > 0 && gameMap[checkIndexInMap(player.tileFrom[0], player.tileFrom[1] - 1)] == 1) {
            player.tileTo[1] -= 1;
            player.animation.change(3);
        } else if (keysDown["ArrowDown"] && player.tileFrom[1] < (rows - 1) && gameMap[checkIndexInMap(player.tileFrom[0], player.tileFrom[1] + 1)] == 1) {
            player.tileTo[1] += 1;
            player.animation.change(0);
        } else if (keysDown["ArrowLeft"] && player.tileFrom[0] > 0 && gameMap[checkIndexInMap(player.tileFrom[0] - 1, player.tileFrom[1])] == 1) {
            player.tileTo[0] -= 1;
            player.animation.change(1);
        } else if (keysDown["ArrowRight"] && player.tileFrom[0] < (columns - 1) && gameMap[checkIndexInMap(player.tileFrom[0] + 1, player.tileFrom[1])] == 1) {
            player.tileTo[0] += 1;
            player.animation.change(2);
        }

        // If it is possible to move to the destination, we update the lastActionTime variable.
        if (player.tileFrom[0] != player.tileTo[0] || player.tileFrom[1] != player.tileTo[1]) {
            updateLastActionTime();
        }
    }

    if (player.tileTo[0] != player.tileFrom[0] || player.tileTo[1] != player.tileFrom[1] || arrowKeyIsPressed()) {
        player.animation.update()
    } else {
        player.animation.remainStatic();
    }

    // To make the center of the player as the center of the view point
    viewPoint.update(player.position[0] + (spriteSize / 2),
        player.position[1] + (spriteSize / 2));

    // To paint the whole canvas as black first before drawing the map
    ctx.fillStyle = "#000000";
    ctx.fillRect(0, 0, viewPoint.canvasSize[0], viewPoint.canvasSize[1]);

    for (let y = viewPoint.startTile[1]; y <= viewPoint.endTile[1]; ++y) {
        for (let x = viewPoint.startTile[0]; x <= viewPoint.endTile[0]; ++x) {
            switch (gameMap[((y * columns) + x)]) {
                case 0:
                    ctx.drawImage(tileSheet, 2 * 32, 36 * 32, 32, 32, viewPoint.offset[0] + (x * tileLength), viewPoint.offset[1] + (y * tileLength),
                        tileLength, tileLength)
                    break;
                default:
                    ctx.drawImage(tileSheet2, 40, 0, 40, 40, viewPoint.offset[0] + (x * tileLength), viewPoint.offset[1] + (y * tileLength),
                        tileLength, tileLength)
            }

        }
    }

    //draw the player
    ctx.drawImage(player.image, player.animation.frameIndex * 32, player.animation.frameSetIndex * 32, spriteSize, spriteSize, viewPoint.offset[0] + player.position[0], viewPoint.offset[1] + player.position[1],
        spriteSize, spriteSize)



    //draw all the monsters that is alive
    for (let monster of allMonster) {

        ctx.drawImage(monster.image, monster.animation.frameIndex * 32, 0, spriteSize, spriteSize, viewPoint.offset[0] + monster.position[0], viewPoint.offset[1] + monster.position[1],
            spriteSize, spriteSize)

        monster.animation.update();
    }

    ctx.fillStyle = "#ff0000";
    ctx.fillText(`Monster Level: ${player.round}`, 20, 36)
    ctx.fillText(`Hero Level: ${player.level.currentLevel}`, 20, 60)
    ctx.fillText(`HP: ${player.hp}`, 20, 84);
}

function arrowKeyIsPressed() {
    if (keysDown["ArrowUp"] || keysDown["ArrowDown"] || keysDown["ArrowLeft"] || keysDown["ArrowRight"]) {
        return true;
    } else {
        return false;
    }
}



//functions that load data from server
async function loadMonster(monsterId) {
    const res = await fetch('/loadMonster', {
        method: 'put',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "monsterId": monsterId
        })
    })
    const result = await res.json()




    currentMonster = result[0]
    let targetMonster = allMonster.filter((monster) => { return monster.id == currentMonster.id })

    currentMonster.img = targetMonster[0].image
}


