import Knex from "knex";
import { expNeed } from "../function";
import * as occ from "../occupation";

export class BattleService {
  constructor(private knex: Knex) {}

  async loadData(playerId: number) {
    const data = await this.knex("characters")
      .select("*")
      .where("player_id", playerId)
      .andWhere("alive", true);

    return data;
  }

  async loadMonster(playerId, monsterId: number) {
    const charId = await this.knex("player")
      .join("characters", "player.id", "characters.player_id")
      .select("characters.id")
      .where("player.id", playerId);

    const data = await this.knex("monsters")
      .select("*")
      .where("id", monsterId)
      .andWhere("character_id", charId[0].id);

    return data;
  }

  async loadAllMonster(playerId: number) {
    const charId = await this.knex("player")
      .join("characters", "player.id", "characters.player_id")
      .select("characters.id")
      .where("player.id", playerId);

    const data = await this.knex("monsters")
      .select("*")
      .where("alive", true)
      .andWhere("character_id", charId[0].id);

    return data;
  }

  async saveBattle(
    playerId: number,
    playerHp: number,
    playerMp: number,
    monsterId: number
  ) {
    const result = await this.knex("monsters")
      .select("location", "expPoint", "gold")
      .where("id", monsterId);

    const pastlevel = await this.knex("characters")
      .update({
        hp: playerHp,
        mp: playerMp,
        location: result[0].location,
      })
      .increment("exp", result[0].expPoint)
      .increment("gold", result[0].gold)
      .where("player_id", playerId)
      .returning("level");

    const characterId = await this.knex("characters")
      .select("id")
      .where("player_id", playerId);

    await this.knex("monsters")
      .update({
        alive: false,
      })
      .where("id", monsterId);

    const monsterAliveCount = await this.checkAllMonsterAlive(
      characterId[0].id
    );

    if (monsterAliveCount == 0) {
      await this.knex("characters")
        .update("location", "[3,3]")
        .increment("round", 1)
        .where("player_id", playerId);

      const result = await this.knex("characters")
        .select("round")
        .where("player_id", playerId);

      const round = result[0].round;
      this.MonsterUpgrade(playerId, round);
    }
    return [result[0], pastlevel];
  }

  async checkOccupation(playerId: number) {
    const result = await this.knex("characters")
      .select("occupation_id")
      .where("player_id", playerId);
    return result;
  }

  async checkMonsterListId(monsterId: number) {
    const result = await this.knex("monsters")
      .select("monsters_list_id")
      .where("id", monsterId);
    return result;
  }

  async checkAllMonsterAlive(characterId: number) {
    const result = await this.knex("monsters")
      .where("alive", "true")
      .andWhere("character_id", characterId)
      .count("alive");

    const count = result[0].count;

    return count;
  }

  async checkLevel(playerId: number) {
    const result = await this.knex("characters")
      .select("exp", "level", "occupation_id")
      .where("player_id", playerId);

    const need = expNeed(result[0].level);
    const currentExp = result[0].exp;

    if (currentExp >= need) {
      await this.knex("characters")
        .increment("level", 1)
        .where("player_id", playerId);

      switch (result[0].occupation_id) {
        case 1:
          this.warriorLevelUp(playerId);
          break;
        case 2:
          this.mageLevelUp(playerId);
          break;
        case 3:
          this.hunterLevelUp(playerId);
          break;
      }
      //check if the player can level more than one level
      return this.checkLevel(playerId);
    }

    return result[0].level;
  }

  async death(playerId: number) {
    await this.knex("characters")
      .update({
        alive: false,
      })
      .where("player_id", playerId);

      const charId = await this.knex("characters")
      .select('id')
      .where('player_id',playerId)

      await this.knex("char-items")
      .where('character_id',charId[0].id)
      .del()
      
  }

  async escapeFunction(
    playerId: number,
    playerHp: number,
    playerMp: number,
    monsterId: number
  ) {
    const location = await this.knex("monsters")
      .select("location")
      .where("id", monsterId);

    await this.knex("characters")
      .update({
        location: location[0].location,
        hp: playerHp,
        mp: playerMp,
      })
      .where("player_id", playerId);
  }

  async warriorLevelUp(playerId: number) {
    await this.knex("characters")
      .where("player_id", playerId)
      .increment("hp", occ.warrior.hpAdd)
      .increment("mp", occ.warrior.mpAdd)
      .increment("atk", occ.warrior.atkAdd)
      .increment("def", occ.warrior.defAdd);
  }

  async mageLevelUp(playerId: number) {
    await this.knex("characters")
      .where("player_id", playerId)
      .increment("hp", occ.mage.hpAdd)
      .increment("mp", occ.mage.mpAdd)
      .increment("atk", occ.mage.atkAdd)
      .increment("def", occ.mage.defAdd);
  }

  async hunterLevelUp(playerId: number) {
    await this.knex("characters")
      .where("player_id", playerId)
      .increment("hp", occ.hunter.hpAdd)
      .increment("mp", occ.hunter.mpAdd)
      .increment("atk", occ.hunter.atkAdd)
      .increment("def", occ.hunter.defAdd);
  }

  async MonsterUpgrade(playerId: number, round: number) {
    const allMonsters = await this.knex("monsters_list").select("*");

    const charactersID = await this.knex("player")
      .join("characters", "player.id", "characters.player_id")
      .select("characters.id")
      .where("player.id", playerId);

    await this.knex("monsters").where("character_id", charactersID[0].id).del();

    for (let monster of allMonsters) {
      await this.knex("monsters").insert({
        character_id: charactersID[0].id,
        name: monster.name,
        monsters_list_id: monster.id,
        hp: monster.hp * round,
        atk: monster.atk * round,
        def: monster.def * round,
        alive: monster.alive,
        location: monster.location,
        img: monster.img,
        expPoint: monster.expPoint,
        gold: monster.gold,
      });
    }
  }

  async loadEquippedItems(charId: number) {
    const atk = await this.knex("char-items")
      .sum("atk")
      .innerJoin("items", "char-items.item_id", "items.id")
      .where("character_id", charId)
      .andWhere("equipped", true);

    const def = await this.knex("char-items")
      .sum("def")
      .innerJoin("items", "char-items.item_id", "items.id")
      .where("character_id", charId)
      .andWhere("equipped", true);
    return [atk[0].sum, def[0].sum];
  }

  async battleUseItem(playerId:number, itemId:number){
     const charId = await this.knex('characters')
     .select('id')
     .where('player_id',playerId)

     await this.knex("char-items")
     .where('character_id',charId[0].id)
     .andWhere('item_id',itemId)
     .decrement('quantity',1)
  }
}
