import Knex from 'knex'
import * as hash from '../hash'
import * as occ from '../occupation'



export class PlayerService {
    constructor(private knex: Knex) { }

    async signUp(body: any) {
        const { userName, displayName, password } = body
        const hashPassword = await hash.hashPassword(password)

        const nameUsed = await this.knex('player')
            .select('*')
            .where('username', userName)

        if (nameUsed[0]) {
            return [false, "UserName in used"]
        } else {
            const playerId = await this.knex('player')
                .insert({
                    username: userName,
                    display_name: displayName,
                    password: hashPassword
                })
                .returning('id')
            return [true, playerId]
        }

    }
    async logIn(body: any) {
        const userName = body
        const result = await this.knex('player')
            .select('*')
            .where('username', userName)

        return result
    }

    async select(body: any[]) {
        const playerId = body[0]
        const occId = body[1]


        const result = await this.knex('characters')
            .select('*')
            .where('player_id', playerId)

        if (result[0] == null) {
            switch (occId) {
                case 1:
                    await this.knex('characters')
                        .insert({
                            level: 1,
                            exp: 0,
                            gold: 0,
                            hp: occ.warrior.hp,
                            mp: occ.warrior.mp,
                            atk: occ.warrior.atk,
                            def: occ.warrior.def,
                            img: occ.warrior.img,
                            location: '[3,3]',
                            name: occ.warrior.name,
                            occupation_id: occId,
                            player_id: playerId,
                            alive: true,
                            round: 1,
                            skill_1: 1,
                            skill_2: 2,
                            skill_3: 3,
                            skill_4: 4
                        })

                    break;

                case 2:
                    await this.knex('characters')
                        .insert({
                            level: 1,
                            exp: 0,
                            gold: 0,
                            hp: occ.mage.hp,
                            mp: occ.mage.mp,
                            atk: occ.mage.atk,
                            def: occ.mage.def,
                            img: occ.mage.img,
                            location: '[3,3]',
                            name: occ.mage.name,
                            occupation_id: occId,
                            player_id: playerId,
                            alive: true,
                            round: 1,
                            skill_1: 1,
                            skill_2: 2,
                            skill_3: 3,
                            skill_4: 4
                        })
                    break;

                case 3:
                    await this.knex('characters')
                        .insert({
                            level: 1,
                            exp: 0,
                            gold: 0,
                            hp: occ.hunter.hp,
                            mp: occ.hunter.mp,
                            atk: occ.hunter.atk,
                            def: occ.hunter.def,
                            img: occ.hunter.img,
                            location: '[3,3]',
                            name: occ.hunter.name,
                            occupation_id: occId,
                            player_id: playerId,
                            alive: true,
                            round: 1,
                            skill_1: 1,
                            skill_2: 2,
                            skill_3: 3,
                            skill_4: 4
                        })
                    break;

            }
        } else if (result[0].alive == false) {
            switch (occId) {
                case 1:
                    await this.knex('characters')
                    .where('player_id',playerId)
                        .update({
                            level: 1,
                            exp: 0,
                            gold: 0,
                            hp: occ.warrior.hp,
                            mp: occ.warrior.mp,
                            atk: occ.warrior.atk,
                            def: occ.warrior.def,
                            img: occ.warrior.img,
                            name: occ.warrior.name,
                            location: '[3,3]',
                            occupation_id: occId,
                            player_id: playerId,
                            alive: true,
                            round: 1
                        })

                    break;

                case 2:
                    await this.knex('characters')
                    .where('player_id',playerId)
                        .update({
                            level: 1,
                            exp: 0,
                            gold: 0,
                            hp: occ.mage.hp,
                            mp: occ.mage.mp,
                            atk: occ.mage.atk,
                            def: occ.mage.def,
                            img: occ.mage.img,
                            location: '[3,3]',
                            name: occ.mage.name,
                            occupation_id: occId,
                            player_id: playerId,
                            alive: true,
                            round: 1
                        })
                    break;

                case 3:
                    await this.knex('characters')
                    .where('player_id',playerId)
                        .update({
                            level: 1,
                            exp: 0,
                            gold: 0,
                            hp: occ.hunter.hp,
                            mp: occ.hunter.mp,
                            atk: occ.hunter.atk,
                            def: occ.hunter.def,
                            img: occ.hunter.img,
                            name: occ.hunter.name,
                            location: '[3,3]',
                            occupation_id: occId,
                            player_id: playerId,
                            alive: true,
                            round: 1
                        })
                    break;

            }
        }

        return { success: true }

    }

    async MonsterSave(playerId: number, round: number) {
        const allMonsters = await this.knex('monsters_list')
            .select('*')

        const charactersID = await this.knex('player')
            .join('characters', 'player.id', 'characters.player_id')
            .select('characters.id')
            .where('player.id', playerId)

        await this.knex('monsters')
            .where('character_id', charactersID[0].id)
            .del()


        for (let monster of allMonsters) {
            await this.knex('monsters')
                .insert({
                    character_id: charactersID[0].id,
                    name: monster.name,
                    monsters_list_id: monster.id,
                    hp: (monster.hp) * round,
                    atk: (monster.atk) * round,
                    def: (monster.def) * round,
                    alive: monster.alive,
                    location: monster.location,
                    img: monster.img,
                    expPoint: monster.expPoint,
                    gold: monster.gold
                })

        }

    }

    async saveData (playerId :number,location:[], skill:any){

        
        const skill_1 = skill[0]
        const skill_2 = skill[1]
        const skill_3 = skill[2]
        const skill_4 = skill[3]


        await this.knex('characters')
        .update({
            location: `[${location}]`,
            skill_1:`${skill_1}`,
            skill_2:`${skill_2}`,
            skill_3:`${skill_3}`,
            skill_4:`${skill_4}`
        })
        .where("player_id",playerId)
    }
}

