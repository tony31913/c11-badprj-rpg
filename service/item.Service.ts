import Knex from "knex";

export class ItemService {
  constructor(private knex: Knex) {}

  async loadCharItems(charId: number) {
    const data = await this.knex("char-items")
      .select("*")
      .where("character_id", charId)
      .innerJoin("items", "char-items.item_id", "items.id");

    return data;
  }

  async loadItemsList() {
    const data = await this.knex("items").select("*");

    return data;
  }

  async loadItemGold(itemId: number) {
    const data = await this.knex("items").select("*").where("id", itemId);
    const result = data[0].gold;
    return result;
  }

  async updateCharCurrency(charId: number, charPrice: number) {
    await this.knex("characters")
      .update({
        gold: charPrice,
      })
      .where("id", charId);
  }

  async buyItems(itemId: number, charId: number) {
    const data = await this.knex("char-items")
      .select("*")
      .where("item_id", itemId)
      .andWhere("character_id", charId);
    if (data.length > 0) {
      await this.knex("char-items")
        .increment("quantity", 1)
        .where("item_id", itemId)
        .andWhere("character_id", charId);
    } else {
      await this.knex("char-items").insert({
        quantity: 1,
        character_id: charId,
        item_id: itemId,
      });
    }

    const allItems = await this.knex("char-items")
      .select("*")
      .andWhere("character_id", charId);

    return allItems;
  }

  async checkCharCurrency(playerId: number) {
    const data = await this.knex("characters")
      .select("gold")
      .where("player_id", playerId);

    const charGold = data[0].gold;
    return charGold;
  }

  async usePotion(charId: number, itemId: number) {
    await this.knex("char-items")
      .decrement("quantity", 1)
      .where("character_id", charId)
      .andWhere("item_id", itemId);
  }

  async updateEquippedStatus(
    charId: number,
    itemId: number,
    equippedStatus: boolean
  ) {
    await this.knex("char-items")
      .update({ equipped: equippedStatus })
      .where("character_id", charId)
      .andWhere("item_id", itemId);
  }

  async selectedPotionValue(itemId: number) {
    const data = await this.knex("items")
      .select("atk", "name")
      .where("id", itemId);
    return data;
  }

  async updateHpMp(charId: number, latestHp: number, latestMp: number) {
    await this.knex("characters")
      .update({ hp: latestHp, mp: latestMp })
      .where("id", charId);
    // data = [{hp:5, mp:30}]
  }
}
