export function expNeed(level: number) {
    const nextlevel = Math.ceil((3 * (level ** 2.2)) / 5 + 10)

    return nextlevel
}


export function bossLogic(occId: number, monsterAtk: number, monsterFullHp: number, monsterHp: number) {

    const monsterHalfHp = monsterFullHp / 2
    const monsterEnrageHp = monsterFullHp / 5
    switch (occId) {
        case 1:
            if (monsterHp < monsterHalfHp && monsterHp > monsterEnrageHp) {
                const random = Math.random()
                if (random < 0.5) {
                    return "Hp Draw"
                } else {
                    return "Normal Attack"
                }

            } else if (monsterHp <= monsterEnrageHp) {
                return "Enerage"
            } else {
                const random = Math.random()
                if (random < 0.3) {
                    return "Hp Draw"
                } else {
                    return "Normal Attack"
                }
            }
        case 2:
            if (monsterHp < monsterHalfHp && monsterHp > monsterEnrageHp) {
                const random = Math.random()
                if (random < 0.5) {
                    return "Mp Draw"
                } else {
                    return "Normal Attack"
                }
            } else if (monsterHp <= monsterEnrageHp) {
                return "Enerage"
            } else {
                const random = Math.random()
                if (random < 0.3) {
                    return "Mp Draw"
                } else {
                    return "Normal Attack"
                }
            }
        case 3:
            if (monsterHp > monsterEnrageHp) {
                const random = Math.random()
                if (random < 0.3) {
                    return "Hp Draw"
                } else if (random > 0.6) {
                    return "Mp Draw"
                } else {
                    return "Normal Attack"
                }

            } else {
                return "Enerage"
            }
        
    }
    return "Normal Attack"
}