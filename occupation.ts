export const warrior = {
    id: 1,
    name: 'Warrior',
    hp: 105,
    mp:50,
    atk: 20,
    def: 4,
    img: './assets/warrior02.png',
    hpAdd: 10,
    mpAdd:10,
    atkAdd: 2,
    defAdd: 2
    
}

export const mage = {
    id: 2,
    name:'Mage',
    hp: 80,
    mp: 300,
    atk: 10,
    def: 1,
    img: './assets/mage02.png',
    hpAdd: 3,
    mpAdd:20,
    atkAdd: 3,
    defAdd: 1
}

export const hunter = {
    id: 3,
    name:'Hunter',
    hp: 95,
    mp:100,
    atk: 15,
    def: 3,
    img: './assets/hunter02.png',
    hpAdd: 6,
    mpAdd:7,
    atkAdd: 2,
    defAdd: 1
}