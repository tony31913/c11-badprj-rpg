interface Item {
    id: number;
    name: string;
    atk: number;
    def: number;
    gold: number;
    type: string;
    image_index: string;
    consumable: boolean
}

export const allitemsList: Item[] = [
    {
        id: 0,
        name: "This is a dummy",
        def: 0,
        atk: 0,
        gold: 0,
        type: "Dummy",
        image_index: "[0, 0]",
        consumable: false
    },
    {
        id: 1,
        name: "Wooden Sword",
        def: 0,
        atk: 10,
        gold: 10,
        type: "Weapon",
        image_index: "[0, 5]",
        consumable: false
    },
    {
        id: 2,
        name: "Wooden Armor",
        def: 10,
        atk: 0,
        gold: 10,
        type: "Clothes",
        image_index: "[6, 7]",
        consumable: false
    },
    {
        id: 3,
        name: "Iron Armor",
        def: 15,
        atk: 0,
        gold: 10,
        type: "Clothes",
        image_index: "[7, 7]",
        consumable: false
    },
    {
        id: 4,
        name: "Iron Axe",
        def: 0,
        atk: 12,
        gold: 10,
        type: "Weapon",
        image_index: "[1, 10]",
        consumable: false
    },
    {
        id: 5,
        name: "Arrow",
        def: 0,
        atk: 10,
        gold: 5,
        type: "Weapon",
        image_index: "[3, 6]",
        consumable: false
    },
    {
        id: 6,
        name: "Iron Sword",
        def: 0,
        atk: 15,
        gold: 10,
        type: "Weapon",
        image_index: "[1, 5]",
        consumable: false
    },
    {
        id: 7,
        name: "Only Shoes",
        def: 10,
        atk: 10,
        gold: 20,
        type: "Shoes",
        image_index: "[2, 8]",
        consumable: false
    },
    {
        id: 8,
        name: "Magic Potion",
        def: 0,
        atk: 10,
        gold: 5,
        type: "Potion",
        image_index: "[2, 9]",
        consumable: true
    },
    {
        id: 9,
        name: "Health Potion",
        def: 0,
        atk: 10,
        gold: 5,
        type: "Potion",
        image_index: "[0, 9]",
        consumable: true
    }

]

